function [ data_clean ] = cleanTS( timeseries,filter_key )
%cleanTS deletes NaNs from time series 
%   Takes time series collection as input. Returns cleaned timeseries
%   collection. Uses 24haverag as filter criteria to check for NaNs.

if strcmp(filter_key,'x24haverage')
    data = timeseries.x24haverage.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'Ask')
    data = timeseries.Ask.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'Bid')
    data = timeseries.Bid.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'Last')
    data = timeseries.Last.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'TotalVolume')
    data = timeseries.TotalVolume.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'Open')
    data = timeseries.Open.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'Close')
    data = timeseries.Close.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'High')
    data = timeseries.High.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'Low')
    data = timeseries.Low.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
elseif strcmp(filter_key,'Volume')
    data = timeseries.Volume.Data;
    idx_NaN = find(isnan(data));
    data_clean = delsamplefromcollection(timeseries,'Index',idx_NaN);
else
    msg = 'Please use a valid filter key. Must be one of the objects field variables';
    disp(msg);
end

