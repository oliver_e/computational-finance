function [r]=simsecSV2(mu,muS,J1,J2,p1,p2,rho,sigmaS,T)
x=randn(T,1); y=rand(T,1); z=randn(T,1); 
J=zeros(T,1); r=zeros(T,1); 
W(1,1)=0; sigma = zeros(T,1);
mu = -0.5*sigma.^2+mu;
for t=2:T
    W(t,1)=rho*W(t-1,1)+sigmaS*z(t,1); 
end
for t=1:T
    sigma(t,1)=abs(muS+W(t,1));  
    if y(t)<p1
       J(t,1)=J1;
    elseif y(t)<p1+p2
       J(t,1)=J2;
    else
       J(t,1)=0;
    end
end
r=exp(mu+sigma.*x+J)-1;
end