function [ stats ] = calcStats( data )
%CALCSTATS Takes array of any numeric data type as input
%-calculates descriptive summary statistics
%--Returns array containing mean, st.dev, skewness, kurtosis in this order
stats = [mean(data);std(data);skewness(data);kurtosis(data)]

end

