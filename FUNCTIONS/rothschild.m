function [ x ] = rothschild( cum1, cum2 )
%ROTHSCHILD just walks along a lorenz curve and checks for rothschild
%stiglitz condition.
%   takes two lorenz functions as input and checks whether they cross at
%   any point. returns 1 if the first cumulative sum is always greater,
%   returns 0 if they cross and -1 if the second is always greater.
% DO NOT USE THIS FUNCTION FROM OUTSIDE. ITS ONLY PURPOSE IS TO CALCULATE
% R&S CONDITION IN DOMINATE!
j=2;
while (j==2 | x==0)
    if cum1(j) > cum2(j)
        x = 1;
    elseif cum1(j) < cum2(j)
        x=-1;
    else
        x = 0;
    end
    j = j+1;
end
if x == 1
  if sum(find(cum1<cum2))>0
      x = 0;
  end
else
    if sum(find(cum1>cum2))>0
        x = 0;
    end
end
      
end

