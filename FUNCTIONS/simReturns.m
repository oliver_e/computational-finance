function [sim_data] = simReturns(mu,sigma,T)
%SIMULATE takes expected value mu, standard deviation sigma and numb of
%days as input. Returns array of simulated data.
mu_cal = mu-0.5*(sigma^2);
sim = randn(T,1);
sim_data = exp(sim*sigma+mu_cal)-1;
end

