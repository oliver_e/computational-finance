function [ dominate ] = dominate( return1, return2)
%DOMINATE checks if lorenz of one curve i lays strictly above curve j 
%   returns matrix of dominance.
[T_c1 T_r1] = size(return1);
[T_c2 T_r2] = size(return2);
if T_c1 == T_c2
    [cum1 rf1] = lorenz(return1);
    [cum2 rf2] = lorenz(return2);
else
    disp('Array length do not match');
end
 dominate = rothschild(cum1,cum2);          
end

