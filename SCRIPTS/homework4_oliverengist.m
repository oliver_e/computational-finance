%Homework 4
%Oliver Engist
%Matr.Nr. 2012-050-225
%---------------------------------------------------------------------
%Read Coke data into workspace
[P_coke Txt_coke] = xlsread('NYSE_KO.xls');
P_coke = P_coke(:,5);
%Calculate Returns
R_coke = calcReturn(P_coke);
r_coke = log(R_coke)
%Stats
mu_coke = mean(R_coke)-1;
sigma_coke = std(R_coke);
[T_coke_r T_coke_c]= size(R_coke);

%Simulate lognorm processed coke Data using the simReturns functions.
sim_R = simReturns(mu_coke,sigma_coke,T_coke_r);
sim_r = log(sim_R+1);

%Simulate 100'000 times (as demanded in class)
sim_100kR = simReturns(mu_coke,sigma_coke,100000);
sim_100kr = log(sim_100kR+1);

%Plotting
subplot(3,1,1);
hist(sim_r,50);
xlabel('Simulated Returns')
subplot(3,1,2);
hist(sim_100kr,50);
xlabel('100k simulated Returns');
subplot(3,1,3);
hist(r_coke,50);
xlabel('Actual Coke log returns');

%Compare means:
stats = [mean(sim_r) mean(sim_100kr) mean(r_coke); std(sim_r) std(sim_100kr) std(r_coke)]

