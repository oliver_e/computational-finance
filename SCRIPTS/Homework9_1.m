%Homework 9
%Oliver Engist
%2012-050-225

clear; clc

% -------------------------------------------------------------------------
% Compute BETA sorted portfolios
% -------------------------------------------------------------------------

clear; clc
load('usstocks200')% loads monthly returns of 200 us stocks
load('rf') % loads the monthly risk-free rate

mkt = mean(ret')'; %equally weighted portfolio as proxy for the market return

ret_rf = ret-repmat(rf,1,200);  % excess returns
mkt_rf = mkt-rf;                % excess market return

% compute rolling betas

% example: first beta
for n=1:200
    [b,bint,r,rint,stats] = regress(ret_rf(1:60,n),[ones(60,1) mkt_rf(1:60,:)]);
    beta(:,n) = b(2,1);
end

% rolling annual betas
%Immer Dezember des Jahres

A = [60:12:396];
for t=1:size(A,2)
    % first beta
    T = A(t);
    for n=1:200 %200 stocks
        [b] = regress(ret_rf(T-60+1:T,n),[ones(60,1) mkt_rf(T-60+1:T,:)]);
        beta(t,n) = b(2,1);
    end
end

%wenn wir portfolios mit spezifischen betas konstruieren w�rde eine aktie
%mit unkonstantem beta �ber die zeit zwischen den portfolios wandern.

% match december betas with the returns in the following year
ret_rf2 = ret_rf(61:end,:); %die daten zur initialen beta berechnung m�ssen wir rausschneiden
mkt_rf2 = mkt_rf(61:end,:); 

%Schleife welche sagt, f�r welche monate wir welches beta verwenden.
for n =1:200
    b  = beta(:,n);
    b2 = repmat(b,1,12)';
    b3 = reshape(b2,348,1);
    beta2(:,n) = b3;
end   

% allocate stocks according to their beta into 10 decile portfolios
% stocks in pofo one have the lowest beta, etc.

% 1) compute break points
break_points = quantile(beta2,[1/10 2/10 3/10 4/10 5/10 6/10 7/10 8/10 9/10],2);

%Die quantile sind wichtig, weil wir danach dann die portfolios
%konstruieren. alle betas die kleiner als das entsprechende quantil sind
%kommen in dieses portfolio. wir haben dann ein 10%, 20%, 30% ...
%portfolio. nun haben wir portfolios mit konstanten betas.
%Wichtig: man muss das nat�rlich f�r jedes jahr neu berechnen.

% 2) compute portfolio returns, allocate stocks in 10 portfolio according
% their beta and the break points
for t = 1:348
    [c,idx] = histc(beta2(t,:).',[-inf break_points(t,:) inf]); % allocates stocks in 10 portfolios in idx, c is the number of stocks in each portfolio 
    pf_rf(t,:) = accumarray(idx,ret_rf2(t,:),[],@mean);         % compute average return for each portfolio
end


for n =1:10
[b] = regress(pf_rf(:,n),[ones(348,1) mkt_rf2]);
beta_pf(1,n)=b(2,:);
end  

%--------------------------------------------------------------------------
% Trading strategy
%--------------------------------------------------------------------------

% 1. Retrieve corner portfolios:
low_beta = beta_pf(1,1);
high_beta = beta_pf(1,10);

high_ret = mean(pf_rf(:,10));
high_std = std(pf_rf(:,10));
low_ret = mean(pf_rf(:,1));
low_std = std(pf_rf(:,10));

%If we use leverage, we have to pay interest. I'll just take the risk free
%rate:
interest = rf(409-348:408);

%Calculate the fraction of portfolio we keep (!) to get a Beta of one if we
%invest the other fraction in a risk free asset.

frac_hi = 1/high_beta;
frac_low = 1/low_beta;

%Returns including interest for debt.
ret_port_hi = pf_rf(:,10)*frac_hi+rf(409-348:408)*(1-frac_hi);
mu_hi = mean(ret_port_hi);
ret_port_low = pf_rf(:,10)*frac_low-interest*(frac_low-1);
mu_low = mean(ret_port_low);

risk_port_hi = std(pf_rf(:,10)*frac_hi+rf(409-348:408)*(1-frac_hi));
risk_port_low = std(pf_rf(:,10)*frac_hi+rf(409-348:408)*(1-frac_hi));

%Combination of portfolios that leads to a overall beta
%of 0:
%Short the high beta stocks and buy low beta stocks.
%Both portfolios with leverage have a Beta of 1 --> short-long equal
%amount.
%Return of this strategy:

ret_strat = mu_low - mu_hi;

%Test for significance: Regression on dummy.
all_ret = [ret_port_hi;ret_port_low];
low_dummy = [zeros(348,1);ones(348,1)];

reg = fitlm(low_dummy,all_ret);

% The Coefficient, which shows the difference of the means, is not
% significant. Our strategy isn't statistically significant profitable.

%Beta of this strategy is zero.
%Alpha?
