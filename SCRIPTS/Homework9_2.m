%homework 9
%Oliver Engist
%2012-050-225

clear; clc
load('usstocks200')% loads monthly returns of 200 us stocks
load('rf') % loads the monthly risk-free rate

mkt = mean(ret')'; %equally weighted portfolio as proxy for the market return

ret_rf = ret-repmat(rf,1,200);  % excess returns
mkt_rf = mkt-rf;                % excess market return

% compute rolling betas

% example: first beta
for n=1:200
    [b,bint,r,rint,stats] = regress(ret_rf(1:60,n),[ones(60,1) mkt_rf(1:60,:)]);
    beta(:,n) = b(2,1);
end

% rolling annual betas
%Immer Dezember des Jahres

A = [60:12:396];
for t=1:size(A,2)
    % first beta
    T = A(t);
    for n=1:200 %200 stocks
        [b] = regress(ret_rf(T-60+1:T,n),[ones(60,1) mkt_rf(T-60+1:T,:)]);
        beta(t,n) = b(2,1);
    end
end


%wenn wir portfolios mit spezifischen betas konstruieren w�rde eine aktie
%mit unkonstantem beta �ber die zeit zwischen den portfolios wandern.

% match december betas with the returns in the following year
ret_rf2 = ret_rf(61:end,:); %die daten zur initialen beta berechnung m�ssen wir rausschneiden
mkt_rf2 = mkt_rf(61:end,:); 

%Schleife welche sagt, f�r welche monate wir welches beta verwenden.
for n =1:200
    b  = beta(:,n);
    b2 = repmat(b,1,12)';
    b3 = reshape(b2,348,1);
    beta2(:,n) = b3;
end   

% allocate stocks according to their beta into 10 decile portfolios
% stocks in pofo one have the lowest beta, etc.

% 1) compute break points
break_points = quantile(beta2,[1/10 2/10 3/10 4/10 5/10 6/10 7/10 8/10 9/10],2);

%Die quantile sind wichtig, weil wir danach dann die portfolios
%konstruieren. alle betas die kleiner als das entsprechende quantil sind
%kommen in dieses portfolio. wir haben dann ein 10%, 20%, 30% ...
%portfolio. nun haben wir portfolios mit konstanten betas.
%Wichtig: man muss das nat�rlich f�r jedes jahr neu berechnen.

% 2) compute portfolio returns, allocate stocks in 10 portfolio according
% their beta and the break points
for t = 1:348
    [c,idxl] = histc(beta2(t,:).',[-inf break_points(t,:) inf]); % allocates stocks in 10 portfolios in idx, c is the number of stocks in each portfolio 
    pf_rf(t,:) = accumarray(idxl,ret_rf2(t,:),[],@mean);         % compute average return for each portfolio
end


%--------------------------------------------------------------------------
% Trading Strategy with underpriced low beta stocks and overpriced high
% beta stocks
%--------------------------------------------------------------------------
%Retrieve Corner Portfolios:
%Construct portfolios for the lowest and highest fraction each year:
[T_c T_r]=size(ret_rf2);
low_return = cell(T_c,1);
low_beta = cell(T_c,1);

high_return = cell(T_c,1);
high_beta = cell(T_c,1);

for i=1:348 
    temp_beta = beta2(i,:);
    quantlow = quantile(temp_beta,0.1);
    quanthigh = quantile(temp_beta,0.9);
    idxl = find(temp_beta<quantlow);
    idxh = find(temp_beta>quanthigh);
    temp_ret = ret_rf2(i,idxl);
    temp_beta = beta2(i,idxl);
    temp_ret_h = ret_rf2(i,idxh);
    temp_beta_h = beta2(i,idxh);
    high_return(i) = mat2cell(temp_ret_h,1,20);
    high_beta(i) = mat2cell(temp_beta_h,1,20);
    low_return(i) = mat2cell(temp_ret,1,20);
    low_beta(i) = mat2cell(temp_beta,1,20);
end

%High and low_ret now contain all the returns for the highest and lowest
%betas in the 10% and 90% qantile.

%Calculate the Beta and mean return for each portfolio:
port_beta_h = ones(T_c,1);
port_beta_l = ones(T_c,1);

port_ret_h = ones(T_c,1);
port_ret_l = ones(T_c,1);

for i=1:T_c
    port_beta_h(i,1) = mean(cell2mat(high_beta(i)));
    port_beta_l(i,1) = mean(cell2mat(low_beta(i)));
    port_ret_h(i,1) = mean(cell2mat(low_return(i)));
    port_ret_l(i,1) = mean(cell2mat(high_return(i)));
end

%Show beta of the corner Portfolio
plot(port_beta_h,'r');
hold on;
plot(port_beta_l,'g');
hold off;
%The Betas aren't stable even if we construct the betas according to their
%Betas.

%Use leverage to make both betas = 1

%If we use leverage, we have to pay interest. I'll just take the risk free
%rate and double it to get a kind of a risk premium.
interest = rf(409-T_c:408)*2;

%If we want every portfolio to have a beta of 1, we have to correct the
%beta by buying more stocks with leverage or sell them and invest in a risk
%free asset.
rf_asset = ones(T_c,1);
leverage = ones(T_c,1);

for i=1:T_c
    rf_asset(i) = 1/port_beta_h(i);
    leverage(i) = 1/port_beta_l(i);
end

%Test whether Beta now equals 1:
betaone_l = port_beta_l.*leverage;
betaone_h = port_beta_h.*rf_asset;

%Return of the portfolio with leverage:
lev_ret_l = port_ret_l.*leverage-interest.*(leverage-ones(T_c,1));
%Plot of the returns.
plot(lev_ret_l);
hold on;
plot(port_ret_l);
hold off;
%Return is now more volatile than without leverage. Seems plausible.

%Return of the high-Beta portfolio with risk free asset.
lev_ret_h = port_ret_h.*rf_asset+rf(409-T_c:408).*rf_asset;
%Plot of the returns with lower Beta.
plot(port_ret_h);
hold on;
plot(lev_ret_h);
hold off;
%Return now less volatile than without risk free asset. Seems also
%plausible.

%Average returns of the portfolios:
mu_low = mean(lev_ret_l);
mu_hi = mean(lev_ret_h);
%Return of the low Beta portfolio exceeds the high beta portfolio by far!

%--------------------------------------------------------------------------
%A trading strategy could be just to buy low beta stocks with leverage.
%At least compared to the high Beta portfolio it performs much better!
%--------------------------------------------------------------------------

%Compare the corner portfolios against the market portfolio.