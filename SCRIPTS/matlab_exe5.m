% Exercise 5

clear; clc; % cleear the workspace and command window

%----
% 1 �
%--------------------------------------------------------------------------
% Use the functions simsec and simsecJ to simulate Microsoft daily returns for a period of a year many times, over and over again (10,000 years)
% Each time record the total return for the year. This is called Monte-Carlo simulation

clear; clc
load('MSFT');

% sigma and mu
sigma=std(R_msft); 
mu=mean(R_msft)-.5*sigma^2; 
T=length(R_msft); 
% jump parameters
nlow=sum(R_msft<-.08);
nhigh=sum(R_msft>.08);
[T a]=size(R_msft);
p1=nlow/T; p2=nhigh/T; 
J1=-.13; J2=.13;


[R_msft_sim]=simsec(mu,sigma,T);

% SIMSEC
T =220; % 220 trading days
for y=1:10000
    [R_msft_sim]=simsec(mu,sigma,T);
    LEVEL = cumprod(1+R_msft_sim);
    R_simsec(y,:) = LEVEL(1,:)/LEVEL(end,:)-1;
end

% SIMSECJ
T =220; % 220 trading days
for y=1:10000
    [R_msft_simJ]=simsecJ(mu,sigma,J1,J2,p1,p2,T);
    LEVEL = cumprod(1+R_msft_simJ);
    R_simsecJ(y,:) = LEVEL(1,:)/LEVEL(end,:)-1;
end

% How likely is it that Microsoft loses 30% in one year? How likely is it to lose 40%? 
disp([sum(R_simsec<-0.30)/10000 sum(R_simsecJ<-0.30)/10000]);

disp([sum(R_simsec<-0.40)/10000 sum(R_simsecJ<-0.40)/10000]);

%----
% 2 �
%--------------------------------------------------------------------------
% Separate the worst 5% of years from the next 95%. What is the value lost at the 5% break? This is called Value at Risk, it is a commonly used risk measure

VaR_simsec = prctile(R_simsec,5);
VaR_simsecJ = prctile(R_simsecJ,5);

% alternative computation
R_simsec_sort=sort(R_simsec);
disp(R_simsec_sort(10000*0.05,:));

disp([VaR_simsec VaR_simsecJ]);

% How sensitive is Value at Risk to jump parameters? Double the jump probabilities and repeat the MC simulation.
% Jump events are quite rare, do you think they are easy to estimate?

% MODEL SENSITIVITIES

% SIMSECJ: DOUBLE JUMP PROBABILITY
T =220; % 220 trading days
for y=1:10000
    [R_msft_simJ2]=simsecJ(mu,sigma,J1,J2,p1*2,p2*2,T);
    LEVEL = cumprod(1+R_msft_simJ2);
    R_simsecJ2(y,:) = LEVEL(1,:)/LEVEL(end,:)-1;
end

VaR_simsecJ2 = prctile(R_simsecJ2,5);


% SIMSECJ: 50% LARGER JUMPS
T =220; % 220 trading days
for y=1:10000
    [R_msft_simJ3]=simsecJ(mu,sigma,J1*1.5,J2*1.5,p1,p2,T);
    LEVEL = cumprod(1+R_msft_simJ3);
    R_simsecJ3(y,:) = LEVEL(1,:)/LEVEL(end,:)-1;
end

VaR_simsecJ3 = prctile(R_simsecJ3,5);

%----
% 3 �
%--------------------------------------------------------------------------
% Extend the model by adding stochastic volatility using simsecSV
% How does this change kurtosis and likelihood of tail events?
% Does Value at Risk change during high volatility and low volatility times?

% stochastic volatiltiy parameters
sigmaS=0.0035; rho=0.99; muS=0.002;

T =220; % 220 trading days
for y=1:10000
    [R_msft_simSV]=simsecSV(mu,muS,J1,J2,p1,p2,rho,sigmaS,T);
    LEVEL = cumprod(1+R_msft_simSV);
    R_simsecSV(y,:) = LEVEL(1,:)/LEVEL(end,:)-1;
end

VaR_simsecSV = prctile(R_simsecSV,5);

% VaR_simsecSV > VaR_simsecJ
% Stochastic Volatility (Volatiltiy Clustering) increases VaR, Tails are getting fatter!

% Do you see any problems with the AR(1) formulation for volatility?

% => Observations more than one period old are ingored, no higher order dependencies are taken into account.
% Now we could estimate an AR(20) process. But then we have to
% calibrate/estimate lots of parameters... :-(

% Better solution: GARCH-Model

% Literature: Robert Engle(2001): "GARCH 101", Journal of Economic Perspectives, Volume 15 (4), 2011, Pages 157�168

