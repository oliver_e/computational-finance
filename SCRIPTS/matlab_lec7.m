clear; clc

% LECTURE 7

% -------------------------------------------------------------------------
load('usstocks200') % loads monthly returns of 200 us stocks

% descriptive statistics
mu_r =mean(ret)*1200; % annualized
std_r = std(ret)*(12^0.5)*100; % annualized
min_r = min(ret)*100;
max_r = max(ret)*100;

subplot(4,1,1); bar(mu_r); xlabel('stock');  title('average return'); 
subplot(4,1,2); bar(std_r); xlabel('stock');  title('average risk'); 
subplot(4,1,3); bar(max_r); xlabel('stock');  title('max return'); 
subplot(4,1,4); bar(min_r); xlabel('stock');  title('min return'); 

disp({'av. return',  'av. vola', 'av. max', 'av. min'});
disp([mean(mu_r) mean(std_r) mean(max_r) mean(min_r)]);

% generate 100 (n) random portfolios of size 1, 2, 3, ..., 50 (A) stocks 
A = [1:1:50];
for i = 1:A(end)
    for n=1:100
        idx = randperm(200); % shuffle the 200 stocks
        %randperm zieht aus einer Menge eine Zahl ohne zurücklegen.
        idx2 = idx(1:A(i)); % pick A stocks
        pf_ret=mean(ret(:,idx2)')';
        if i==1 %Sonderfall weil i==1 ein problem mit dem transponieren ist
            pf_ret=ret(:,idx2);
        end
        mu_pf_n(n,i) = mean(pf_ret);
        std_pf_n(n,i) = std(pf_ret);
    end
end
mu_pf = mean(mu_pf_n)*1200; 
std_pf = mean(std_pf_n)*(12^0.5)*100;

subplot(2,1,1); bar(mu_pf); xlabel('# stocks in portfolio'); ylabel('%, p.a.'); title('average return'); 
subplot(2,1,2); bar(std_pf); xlabel('# stocks in portfolio'); ylabel('%, p.a.'); title('volatility'); 

disp('Return and Risk of 1 Stock Portfolios:')
disp([mu_pf(1) std_pf(1)])

disp('Return and Risk of 50 Stocks Portfolios:')
disp([mu_pf(50) std_pf(50)])

% If you buy a number   of stocks, the portfolio will have the same
% return as single stocks but only half of the standard deviation / risk!

%-------------------------------------------------------------------------- 
% Variance decomposition
%--------------------------------------------------------------------------
%   -> How much of the stock risk is "common" among stocks?
%   -> How much is idiosyncratic?

figure;
bar(std_pf); xlabel('# stocks in portfolio'); ylabel('%, p.a.'); title('volatility'); 

ret_EW = mean(ret')'; % return of an equally weighted portfolio (max diversified)

%Calculate the Beta for each stock and the intercept.
for n=1:200
    b = regress(ret(:,n),[ones(408,1) ret_EW]);
    beta(:,n) = b(2,1);
end

bar(beta);
disp(mean(beta))

%Den Achsenabschnitt ignorieren wir momentan noch!

for n= 1:200
    e(:,n) = ret(:,n)-beta(:,n).*ret_EW;
end

var_idiosyncratic = var(e);
var_total = var(ret);
var_systematic = var_total - var_idiosyncratic;
var_systematic_check = (beta.^2).*var(ret_EW);

bar(var_idiosyncratic./var_total*100);
disp(mean(var_idiosyncratic./var_total*100));

% 3/4 of the stock market variance is idiosyncratic! 
    
%--------------------------------------------------------------------------
% simulate multiple securities
%--------------------------------------------------------------------------

% selected empirical properties of returns
disp(mean(std(ret)))
disp(std(std(ret)))
%Covarianzmatrix
cov_empirical = cov(ret);
%Zeige nur die ersten 5x5 covarianzen. Ganze Matrix ist 200x200
disp(cov_empirical(1:5,1:5)*100);

% no systematic risk %-----------------------------------------------------
N=200; T=408;

mui=ones(1,N)*0;   % at the moment, we focus on modelling the covariance matrix             
sigmai=0.0817+0.0214*randn(1,N);    %Mittlere Standardabweichung + Zufallswert      

for i=1:N;
    ret_sim(:,i)= mui(i)+sigmai(i)*randn(T,1);
end;

disp(mean(std(ret_sim)))
disp(std(std(ret_sim)))

A = [1:1:50];
for i = 1:A(end)
    for n=1:100
        idx = randperm(200); % shuffle the 200 stocks
        idx2 = idx(1:A(i)); % pick A stocks
        pf_ret=mean(ret_sim(:,idx2)')';
        if i==1
            pf_ret=ret_sim(:,idx2);
        end
        mu_pf_n(n,i) = mean(pf_ret);
        std_pf_n(n,i) = std(pf_ret);
    end
end

cov_no_risk = cov(ret_sim);
disp(cov_no_risk(1:5,1:5)*100);

std_pf_no_risk = mean(std_pf_n)*(12^0.5)*100;

bar(std_pf_no_risk); % annualized standard deviation
xlabel('# stocks in portfolio'); % x-axis lable
ylabel('%, p.a.'); title('volatility'); 
hold on;    
%Problem: Weil wir kein aggregiertes (systematisches) Risiko dabei haben
%konvergiert das Risiko gegen Null. Der Achsenabschnitt muss quasi noch um
%das Systematische Risiko nach oben verschoben werden.

% adding systematic risk %-------------------------------------------------
std(ret_EW)

N=200; T=408;
mui=ones(1,N)*0;                  
sigmai=(0.0817^2-0.0432^2)^0.5+0.0214*randn(1,N);             
Y=.0432*randn(T,1);

for i=1:N;
    %Simulierte Returns mit dem modellierten Risiko.
    ret_sim(:,i)= mui(i)+sigmai(i)*randn(T,1)+beta(i)*Y;
end;

disp(mean(std(ret_sim)))
disp(std(std(ret_sim)))

A = [1:1:50];
for i = 1:A(end)
    for n=1:100
        idx = randperm(200); % shuffle the 200 stocks
        idx2 = idx(1:A(i)); % pick A stocks
        pf_ret=mean(ret_sim(:,idx2)')';
        if i==1
            pf_ret=ret_sim(:,idx2);
        end
        mu_pf_n(n,i) = mean(pf_ret);
        std_pf_n(n,i) = std(pf_ret);
    end
end

cov_with_risk = cov(ret_sim);
disp(cov_with_risk(1:5,1:5)*100);

std_pf_with_risk = mean(std_pf_n)*(12^0.5)*100;

bar(std_pf_with_risk); % annualized standard deviation
xlabel('# stocks in portfolio'); % x-axis lable
ylabel('%, p.a.'); title('volatility'); 
hold on;    

% Compare results without risk to results with risk
figure;
plot(std_pf_no_risk, 'b','LineWidth',3); hold on
plot(std_pf_with_risk, 'r','LineWidth',3); 
xlabel('# stocks in portfolio'); % x-axis lable
ylabel('%, p.a.'); title('volatility'); 


%--------------------------------------------------------------------------
% Principal Component Analysis (PCA)
%--------------------------------------------------------------------------

% The  idea behind PCA  is that the variation of a set of observable variables can be attributed to common
% underlying forces.
% Example: Stock market returns are all affected by overall market movements.

% PCA is a  purely statistical procedure, which CAN be helpful to identify  economically interesting (systematic) factors.
% It transforms the the variables of interest in a set of new variables called principal components.
% The transformation is done such that the principal components are orthogonal (uncorrelated) with each other.

% Example:
[PCA_coef, PCA_lat, PCA_expl] = pcacov(corr(ret)); % use the correlation matrix to correct for different volatilities

% investigate the weights of the first PCA:
bar(PCA_coef(:,1)); 
% They are all positiv and similar in magnitude, like the weights of the market portfolio

%  The principal components are computed as PCA-Coefficient * Returns, "PCA_coef" can be interpreted as portfolio weights
PCA1 = ret*PCA_coef(:,1);      % First Principal Component

w = PCA_coef(:,1)./std(ret)';  % Better to divide PCA coefficients to correct for different volatilities
PCA1 = ret*(w./sum(w));        % normalize portfolio weights such that they sum to one 

disp(corr(PCA1,ret_EW)); 
% Indeed, PCA1 and the equally weigthed portfolio return are highly correlated with each other
% We have a plausible economic interpretation of the first PC!

% PCA_expl tells you how much of the variance of the returns is explained by each principal component

% The PCs are constructed such that the first PC explains most of the
% variance, followed by the second PC, followed by the third PC, etc....
disp('Variance explained by the first five PC');
disp(PCA_expl(1:5,:));
% The first PC explains 30% of the variance of the returns and is by far the
% most important factor

    % What does "explains 30% of the variance" mean?
    for n=1:200
        b = regress(ret(:,n),[ones(408,1) PCA1]);
        beta(:,n) = b(2,1);
        e(:,n) = ret(:,n)-beta(:,n).*PCA1;
    end
    disp(mean((1-var(e)./var(ret)))*100) 
    disp(mean((var(e)./var(ret)))*100) 
    % This is the same variance decomposition  we did before!
    


