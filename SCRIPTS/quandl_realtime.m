clear; clc;
%Trial file for quandl realtime access:

Quandl.auth('uYWr29dcdx1Hsf3JmVtW');

bitcoin = Quandl.get('BAVERAGE/USD', 'trim_start', '2010-07-17', 'trim_end', '2014-10-25', 'authcode', 'uYWr29dcdx1Hsf3JmVtW');
%Quandl GET command returns a matrix of time-series objects
%TS Objects have fields for Last, Ask, Bid and x24hAverage.
%If we retrieve first row of the matrix, we get 24h average.


%Convert time series objects into regular Array.
ask_bitc = bitcoin.Ask.Data;
bid_bitc = bitcoin.Bid.Data;



%clean NaN
idx_NaN = find(isnan(ask_bitc));
ask_bitc(idx_NaN)=[];
bid_bitc(idx_NaN)=[];

bitcoin_clean = delsamplefromcollection(bitcoin,'Index',idx_NaN);

%Plot
plot(ask_bitc);
hold on;
plot(bid_bitc);
hold off;

%--------------------------------------------------------------------------
%Another Asset (MMM)

MMM = Quandl.get('GOOG/NYSE_MMM', 'trim_start', '1981-03-11', 'trim_end', '2014-10-24', 'authcode', 'uYWr29dcdx1Hsf3JmVtW')

MMM_clean = cleanTS(MMM,'Close');
