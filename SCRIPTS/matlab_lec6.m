clear; clc;

% LECTURE 6

%--------------------------------------------------------------------------
% simulation - GARCH

w=.05; a=.1; b=.8; T=1000;
[R_simGARCH SIGMAsq_simGARCH]=simGARCH(w,a,b,T);

%Time-series
subplot(3,1,1); plot(R_simGARCH); title('R');
subplot(3,1,2); plot(R_simGARCH.^2); title('R^{2}');
subplot(3,1,3); plot(SIGMAsq_simGARCH); title('\sigma^{2}');

% estimate GARCH on simulated data
T=100000;
[R_simGARCH SIGMAsq_simGARCH]=simGARCH(w,a,b,T);
clear X; n=100;
%X is a vector, containing the squared returns
for i=1:n;
    X(:,i)=R_simGARCH(n-i+1:T-i,1).^2;
end;
Y=R_simGARCH(n+1:T,1).^2;
regcoef=regress(Y,[ones(T-n,1) X]);
aest=regcoef(2);
best=regcoef(3)/regcoef(2);
west=regcoef(1)/((1-best^n)/(1-best));
disp([w a b;west aest best])


% MATLAB build-in
[west2 best2 aest2] = ugarch(Y,1,1);

% Kevin Sheppard, MFE Toolbox
param3 = tarch(Y,1,0,1);

disp([w a b;west aest best; west2 aest2 best2; param3']);

% repeat this exercise with a lower T, eg T=1000! What happens to the different estimators?


%--------------------------------------------------------------------------
% GARCH estimation for microsoft

clear; clc
load('MSFT');

% sigma and mu
sigma=std(R_msft); mu=mean(R_msft)-.5*sigma^2; T=length(R_msft); 
% jump parameters
nlow=sum(R_msft<-.08);
nhigh=sum(R_msft>.08);
[T a]=size(R_msft);
p1=nlow/T; p2=nhigh/T; 
J1=-.13; J2=.13;
% stochastic volatiltiy parameters
sigmaS=0.0035; rho=0.99; muS=0.002; % also try rho = 0.9999
% simulate SV model!
[R_msft_simSV]=simsecSV(mu,muS,J1,J2,p1,p2,rho,sigmaS,T);

% plot data
startDate = datenum('01-03-2000');
endDate = datenum('01-02-2014');
time_axis = date_num_msft(2:end,:); % interpolation between starting and end date
subplot(2,1,1); plot(time_axis,R_msft(1:T));
datetick('x','yyyy','keepticks');
title('Microsoft'); axis([time_axis(1) time_axis(T) -.2 .2]);
subplot(2,1,2); plot(time_axis,R_msft_simSV(1:T)); 
datetick('x','yyyy','keepticks');
title('Simulated'); axis([time_axis(1) time_axis(T) -.2 .2]);

% estimate msft
param_msft = tarch(R_msft-mean(R_msft),1,0,1);
param_sim = tarch(R_msft_simSV-mean(R_msft_simSV),1,0,1);

disp([param_msft'; param_sim']);

% estimated vola
w=param_msft(1);
a=param_msft(2);
b=param_msft(3);

SIGMA_msft=zeros(T,1);

n=100;
for t=n+1:T;
    k=0; s=0;
    for i=1:n;
        k=k+b^(i-1);
        s=s+(R_msft(t-i)^2)*b^(i-1);
    end;
    SIGMA_msft(t)=sqrt(w*k+a*s);
end;

SIGMA_msft(1:n)=mean(SIGMA_msft(n+1:T));
subplot(2,1,1); plot(time_axis,R_msft(1:T));
title('Microsoft Return'); axis([time_axis(1) time_axis(T) -.2 .2]);
datetick('x','yyyy','keepticks');
subplot(2,1,2); plot(time_axis,SIGMA_msft(1:T));
title('Microsoft Volatility'); axis([time_axis(1) time_axis(T) 0 .06]);
datetick('x','yyyy','keepticks');

% functions which estimate GARCH  models usually provide the fitted volatility
% as an optional output
[param_msft ll sigmasq_msft]= tarch(R_msft-mean(R_msft),1,0,1);
SIGMA_msft2 = sqrt(sigmasq_msft);
plot([SIGMA_msft SIGMA_msft2])


%--------------------------------------------------------------------------
% VaR 

T0=100000; sigma=std(R_msft);
x=randn(T0,1)*sigma;

% normcdf:  returns the standard normal cdf at each value in x
p = normcdf(-0.01,0,sigma);

% norminv: computes the inverse of the normal cdf using the corresponding mean mu and standard deviation sigma at the corresponding probabilities in P
R_p = norminv(p,0,sigma);

% VaR example, normal distribution
mult_nd = norminv(0.05,0,1);
R_VaR5 = sigma*mult_nd;

hist(x,50); axis([-.15 .15 0 8000]); hold on;
plot(R_VaR5*ones(6,1), 0:8000/5:8000,'r','LineWidth',3);

disp([R_VaR5 sum(x<R_VaR5)/T0]);

% Backtesting
VaR5_msft=SIGMA_msft.*mult_nd;

subplot(2,1,1); plot(time_axis,SIGMA_msft);
datetick('x','yyyy','keepticks');
title('MSFT Volatility'); axis([time_axis(1) time_axis(T) 0 .06]);
subplot(2,1,2); plot(time_axis,VaR5_msft);
datetick('x','yyyy','keepticks');
title('MSFT VAR 5%'); axis([time_axis(1) time_axis(T) -.1 0]);

disp('Percent Violations GARCH VAR 5%');
disp(sum(R_msft<VaR5_msft)/T);

VaR5_msft_const_vola=std(R_msft).*mult_nd;
disp('Percent Violations Constant Vol VAR 5%');
disp(sum(R_msft<VaR5_msft_const_vola)/T);

% Both overestimate MSFT's number of extreme returns (atypical when using normal distribution!)
% GARCH model is more conservative (typical)

% VaR example, empirical distribution
e=R_msft./SIGMA_msft2; % standardized residuals
mult_empd = prctile(e,5); % 5% quantile of the empirical distribution
VaR5_msft=SIGMA_msft.*mult_empd;

disp('Percent Violations GARCH VAR 5%');
disp(sum(R_msft<VaR5_msft)/T);

% Indeed, literature finds that the tail behavior of GARCH models remains too short 
% Many extensions are proposed in the literature, e.g. E-GARCH, T-GARCH,
% ... (http://en.wikipedia.org/wiki/Autoregressive_conditional_heteroskedasticity)

        