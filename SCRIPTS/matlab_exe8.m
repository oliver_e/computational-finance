% Exercise 8

clear; clc; % cleear the workspace and command window

%----
% 1 �
%--------------------------------------------------------------------------
% Add short sales constraints to the portfolio optimization problem

%Hint: 
%define A=diag(ones(1,N),0); Z=zeros(N,1)
%Then we can write short sale constraints as the inequality: A w >= Z

% Load data
load('usstocks200') % loads monthly returns of 200 us stocks

options=[];
options=optimset(options,'LargeScale','off');
options=optimset(options,'TolFun',1e-15); % <- this option is important!
options=optimset(options,'Display','off');

% compute the mean variance frontier
tic
%N=4; 
 N = 100; 
% N= 200;  

E = mean(ret(:,1:N))';      % average returns
S = std(ret(:,1:N))';
V = cov(ret(:,1:N));        % variance-covariance matrix
w_naive = ones(N,1)/N;  % naive portfolio weights 

% plot
scatter(S, E)
axis([0 0.15 0 0.026])
xlabel('risk');
ylabel('return'); 
hold on

% short sale constraints
A=diag(ones(1,N),0);  
Z=zeros(N,1);

%r_grid = [0:0.0001:0.022]';
r_grid = [min(E):0.0001:max(E)]';
for i =1:size(r_grid,1)
    w0 =w_naive; 
    w=fmincon(@(w) w'*V*w,w0,-A,Z,[E'; ones(1,N)],[r_grid(i,:); 1],[],[],[],options); % without short sales
    % w=fmincon(@(w) w'*V*w,w0,[],[],[E'; ones(1,N)],[r_grid(i,:); 1],zeros(N,1),[],[],options); % alternativ: use lower bound input
    P_E1(i,:) = w'*E;
    P_S1(i,:) = (w'*V*w)^0.5;
    W(:,i) = w;     % matrix with portfolio weights
end 
plot(P_S1, P_E1, 'g');

% sharpe ratio: (E-rf)/std;
% assume rf = 0.001 (=1.2% p.a.)
for i =1:size(r_grid,1)
    sharperatio(i,1)=(P_E1(i,1)-0.001)/P_S1(i,1);
end
i_tangency = find(sharperatio == max(sharperatio));
TP_E = P_E1(i_tangency,:);
TP_S = P_S1(i_tangency,:);
scatter(TP_S, TP_E, 'gd','filled');
text(TP_S, TP_E,'TP','horizontal','left', 'vertical','bottom')

% draw the tangent
line([0,max(P_S1)],[0.001,0.001+(TP_E-0.001)/TP_S*max(P_S1)]) ;
toc

%----
% 2 �
%--------------------------------------------------------------------------
%Compare the mean-variance efficient frontier without short sale constraints to the mean-variance efficient frontier with short sales constraints

r_grid = [0:0.0001:0.022]';
for i =1:size(r_grid,1)
    w0 =w_naive; 
    w=fmincon(@(w) w'*V*w,w0,[],[],[E'; ones(1,N)],[r_grid(i,:); 1],[],[],[],options); % with short sales
    P_E2(i,:) = w'*E;
    P_S2(i,:) = (w'*V*w)^0.5;
    W(:,i) = w;     % matrix with portfolio weights
end 
plot(P_S2, P_E2, 'r--');


%----
% 3 �
%--------------------------------------------------------------------------
% Bonus: Compute the value-weighted market portfolio

% value weighted market portfolio:
% w_{i;t} = me_{i;t}/sum(me_{i;t})
% mkt_vw = sum( r_{i;t} * w_{i;t-1} ) 

w = [];
for t=1:408
    w(t,:)=me(t,:)./repmat(sum(me(t,:)),1,200);
end
bar([w([1 407],:)]')

mkt_vw = sum(w(1:end-1,:).*ret(2:end,:),2);

% for comparison
mkt_ew = mean(ret(2:end,:),2);
disp([mean(mkt_vw) mean(mkt_ew);std(mkt_vw) std(mkt_ew)])

scatter(std(mkt_vw), mean(mkt_vw), 'kd','filled');
text(std(mkt_vw), mean(mkt_vw),'MKT(VW)','horizontal','left', 'vertical','bottom')

scatter(std(mkt_ew), mean(mkt_ew), 'yd','filled');
text(std(mkt_ew), mean(mkt_ew),'MKT(EW)','horizontal','left', 'vertical','bottom')
