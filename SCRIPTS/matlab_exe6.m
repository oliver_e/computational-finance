% Exercise 6

clear; clc; % cleear the workspace and command window

%----
% 1 �
%--------------------------------------------------------------------------
% Estimate the GARCH model for the first 2269 observations (~2/3 of the sample) of microsoft returns (R_msft).

load('MSFT');

R_msft_est = R_msft(1:2269,:);
R_msft_fcast = R_msft(2270:end,:);
    
param_msft_full = tarch(R_msft-mean(R_msft),1,0,1);                 % for comparison
[param_msft ll ssq_msft] = tarch(R_msft_est-mean(R_msft_est),1,0,1);% we are intresested in these parameters
param_msft_fcast = tarch(R_msft_fcast-mean(R_msft_fcast),1,0,1);    % for comparison
disp([param_msft'; param_msft_full';param_msft_fcast' ]);

% estimates differ for the full sample, the estimation window, and the forecast window!

% estimated vola
w=param_msft(1);
a=param_msft(2);
b=param_msft(3);


%----
% 2 �
%--------------------------------------------------------------------------
% Make a forecast for the VaR(5%) for the period 2270. Then make a forecast for the following period, and so forth, until the end of the sample (period 3269).
% Take care that you only use information which was available at the time the forecast was made. This procedure is called out-of-sample testing. 

% sigma_{t}^2 = w + a*r_{t-1} + b * sigma_{t-1}^2

R_msft_sq = R_msft.^2; % squared returns
R_msft_fcast_sq = R_msft_fcast.^2; % squared returns

% forecast for period 2270
sigma_sq_2270 = w + a * R_msft_sq(2269,:) + b * ssq_msft(end,:);

% other periods
sigma_sq_fcast(1) = sigma_sq_2270;
for t=2:1000
    sigma_sq_fcast(t) =  w + a * R_msft_fcast_sq(t-1,:) + b * sigma_sq_fcast(t-1);
end

mult_nd = norminv(0.05,0,1);
VaR5_fcast = (sigma_sq_fcast.^0.5)*mult_nd;

disp('Percent Violations GARCH VAR 5%');
disp(sum(R_msft_fcast<VaR5_fcast')/1000);

% results are too conservative
   
%----
% 3 �
%--------------------------------------------------------------------------
% Compare the GARCH model with the constant volatility model out-of-sample. 

% constant volatility model
VaR5_cv_fcast=std(R_msft_est).*mult_nd;

disp('Percent Violations Constant Vol. VAR 5%');
disp(sum(R_msft_fcast<VaR5_cv_fcast')/1000);

% Problem: MSFT returns are in the forecasting period less volatile then in
% the estimation period
disp([std(R_msft_est) std(R_msft_fcast)])

% Intuitively, the GARCH model is  more robust to time-varying volatility


%--------
% Bonus �
%--------------------------------------------------------------------------
% Is the out-of-sample performance of the GARCH model improved when you update the model estimates in each period?

% you reestimate the GARCH model ech period

% other periods
sigma_sq_fcast = [];
sigma_sq_fcast(1) = sigma_sq_2270;

%expanding window
for t=2:1000
    % 1) reestimation of the GARCH model using all available information at time t-1
    [param_msft] = tarch(R_msft(1:2269+t-1,:)-mean(R_msft(1:2269+t-1,:)),1,0,1);
    w=param_msft(1);
    a=param_msft(2);
    b=param_msft(3);
    % 2) forecast for period t as before
    sigma_sq_fcast(t) =  w + a * R_msft_fcast_sq(t-1,:) + b * sigma_sq_fcast(t-1);
end

mult_nd = norminv(0.05,0,1);
VaR5_fcast = (sigma_sq_fcast.^0.5)*mult_nd;

disp('Percent Violations GARCH VAR 5%');
disp(sum(R_msft_fcast<VaR5_fcast')/1000);
% slightly better

% There could be an impact of the dotcom boom and bust on MSFT returns which
% we do not want to take into account when estimating the VaR of MSFT in 2011 

%  alternative: three years rolling window (i.e. only take the past three years of data into account)
for t=2:1000
    % 1) reestimation of the GARCH model using all available information at time t-1
    [param_msft] = tarch(R_msft(2269+t-660:2269+t-1,:)-mean(R_msft(2269+t-660:2269+t-1,:)),1,0,1);
    w=param_msft(1);
    a=param_msft(2);
    b=param_msft(3);
    % 2) forecast for period t as before
    sigma_sq_fcast(t) =  w + a * R_msft_fcast_sq(t-1,:) + b * sigma_sq_fcast(t-1);
end

mult_nd = norminv(0.05,0,1);
VaR5_fcast = (sigma_sq_fcast.^0.5)*mult_nd;

disp('Percent Violations GARCH VAR 5%');
disp(sum(R_msft_fcast<VaR5_fcast')/1000);
% even better