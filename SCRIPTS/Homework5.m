%Oliver Engist
%2012-050-225
%Computational Finance

%//////////////////////////////////////////////////////////////////////////
%USES external functions
%calcStats, calcReturns, simsec, simsecJ2, simsecSV
%//////////////////////////////////////////////////////////////////////////

clear; clc
%--------------------------------------------------------------------------
%Preparation
%--------------------------------------------------------------------------

%Load and read Microsoft Data:
[MSFT_data MSFT_txt]=xlsread('NASDAQ_MSFT.xls');

MSFT_close = MSFT_data(:,5);

%Save as a matrix
save('MSFT_close');

%Calculate Returns
R_msft = calcReturn(MSFT_close)-1;
[T_r T_c]= size(R_msft);

%Stats
stats_msft = calcStats(R_msft);

%Plot
subplot(2,2,1);
hist(R_msft,50);


%--------------------------------------------------------------------------
%Simulation
%I put the calibration of mu inside the simsec function. Too dangerous
%otherwise.
%--------------------------------------------------------------------------

T = 10000;           %Years
days = 250;         %Trading Days

sim_y = zeros(T,1);
sim_d = zeros(days,1);
s = ones(days,1);

mu_msft = stats_msft(1);
sigma_msft = stats_msft(2);

%Simulate 250 returns for every year. Log them and sum them.
for i=1:T
    sim_d = log(simsec2(mu_msft, sigma_msft ,days)+1);
    ann = sim_d'*s;
    if ann>-1
        sim_y(i) = ann;
    else
        sim_y(i) = -1;
    end
end
  
subplot(2,2,2);
hist(sim_y,50);
xlabel('Simulated ANNUAL return');
sim_y_stats = calcStats(sim_y);     %Kurtosis doesn't yet match.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Problem: Sum of log daily returns can be < -1.....%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%--------------------------------------------------------------------------
%Simulation Part II
%-Include fat tails by including jumps
%--------------------------------------------------------------------------

sim_dJ = zeros(days,1);
sim_yJ = zeros(T,1);

%Probabilities for large outliers.
%I treat values > mu+4*sigma as outliers.
downer = R_msft<(mu_msft-4*sigma_msft);
upper = R_msft>(mu_msft+4*sigma_msft); 

out_pos = sum(upper);
out_neg = sum(downer);

pJ_p = out_pos/T_r;
pJ_n = out_neg/T_r;

J1 = mean(R_msft(downer))*2;
J2 = mean(R_msft(upper))*2;

%Simulate annual log Returns for 10'000 years with jumps using simsecJ.
%I changed the provided functions. If there are problems, running the file,
%this might be a potential source.

tic;
for i=1:T
    sim_dJ = log(simsecJ2(mu_msft,sigma_msft,J1,J2,pJ_n,pJ_p,days)+1);
    ann = sim_dJ(:,1)'*s;
    if ann>-1
        sim_yJ(i) = ann;
    else
        sim_yJ(i) = -1;
    end
end
toc;
sim_yJ_stats = calcStats(sim_yJ);

subplot(2,2,3);
hist(sim_yJ,50);
xlabel('ANNUAL return with jumps');

%Result: The kurtosis doesn't fit because Jumps in the daily returns don't
%affect the annual average very much. The annual average is a result of 250
%days, calculated for 10'000 years, while the original MSFT data only 
%consists of 2203 single observations.
%As a consequence, we don't have fat tails in large simulated datasets.

%--------------------------------------------------------------------------
%Value at Risk
%--------------------------------------------------------------------------

%Likelihood of losing 30% in one year:
down30 = sum(sim_yJ<-0.3);
p_down30 = down30/T;

%Likelihood of losing 40% in one year:
down40 = sum(sim_yJ<-0.4);
p_down40 = down40/T;

%Sort the simulated returns to retrieve the 5% quantile.
sim_yJsort = sort(sim_yJ);

%Find the annual return of the best year within the 5% range.
index_05 = T*0.05;
quant_05 = sim_yJsort(index_05);

%The VaR would be the return of the 5% quantile multiplied by the invested
%capital.
%With the initial jump probability, the 5% quantile is -0.5716.
%If I double the probability, the 5% quantile is -0.6468

%I think this is a strong reaction for annual data. Doubling the
%probabilities of large outliers may definitely happen because the overall
%number of them is very low.

%I find it too hard to estimate this probability. The number of times such
%events occur is very small and already a few more or less such events has
%a large influence on the distribution in a small sample.
%On the other hand, distributions like the simulated one seem to be somehow
%robust against outliers. The kurtosis doesn't change much even with very
%large jumps and high probabilities. Only the VaR reacts sensitively.

%--------------------------------------------------------------------------
%Simulation with Jumps again
%-Include stochastic volatility
%--------------------------------------------------------------------------

%Build 20d intervals:
T1=floor(T_r/20); ma=zeros(T1,1);
for i=1:T1;
    % 20 day standard deviation
    in=(i-1)*20+1:(i-1)*20+20;
    ma(i,1)=std(R_msft(in,1));
end;

% predictability regressions
% X ist die Koeffizientenmatrix [1 x]
% Y ist die abhängige Varible. Hier die Volatilität des nächsten Tages.
% Die Regression erklärt heutige Volatilität durch Volatilität des Vortags.
% Deshalb stammen x und y aus derselben Matrix.

% actual data
X=[ones(T1-1,1) ma(1:T1-1,1)];
Y=ma(2:T1,1);
%regcoeff is a vector, containing the coefficients of the linear model
%sterr is a vector containing the 95% intervals.

[regcoef sterr c d rsq]=regress(Y,X);
disp([regcoef sterr]);
disp(rsq(1));

% simulation with stochastic volatility
sigmaS=0.0035; rho=0.99; muS=sigma_msft;

sim_ySV = zeros(T,1);
one_vec = ones(days,1);
for i=1:T
    ann = 1;
    R_msft_simSV=log(simsecSV2(mu_msft,muS,J1,J2,pJ_n,pJ_p,rho,sigmaS,days)+1);
    ann = R_msft_simSV'*one_vec;
    if ann>-1
        sim_ySV(i,1) = ann;
    else
        sim_ySV(i,1) = -1;
    end
end
sim_ySV_stats = calcStats(sim_ySV);
subplot(2,2,4);
hist(sim_ySV,50);
xlabel('ANNUAL return w/ jumps and stoch. vol.')

%Conclusion: with stochastic volatility, the kurtosis doesn't increase.
%Maybe because annual return has a lower bound of -1.

%VaR
%Sort the return array
sim_ySV_sort = sort(sim_ySV);
quant_05SV = sim_ySV_sort(index_05);

%The VaR at the 5% Level has decreased from -0.5671 without stoch. vola to 
%-0.76.