%Homework 9
%Oliver Engist
%2012-050-225

clear; clc

load('usstocks200')% loads monthly returns of 200 us stocks
load('rf') % loads the monthly risk-free rate

mkt = mean(ret')'; %equally weighted portfolio as proxy for the market return

ret_rf = ret-repmat(rf,1,200);  % excess returns
mkt_rf = mkt-rf;                % excess market return

% compute rolling betas

% example: first beta
for n=1:200
    [b,bint,r,rint,stats] = regress(ret_rf(1:60,n),[ones(60,1) mkt_rf(1:60,:)]);
    beta(:,n) = b(2,1);
end

% rolling annual betas
%Immer Dezember des Jahres

A = [60:12:396];
for t=1:size(A,2)
    % first beta
    T = A(t);
    for n=1:200 %200 stocks
        [b] = regress(ret_rf(T-60+1:T,n),[ones(60,1) mkt_rf(T-60+1:T,:)]);
        beta(t,n) = b(2,1);
    end
end

%wenn wir portfolios mit spezifischen betas konstruieren w�rde eine aktie
%mit unkonstantem beta �ber die zeit zwischen den portfolios wandern.

% match december betas with the returns in the following year
ret_rf2 = ret_rf(61:end,:); %die daten zur initialen beta berechnung m�ssen wir rausschneiden
mkt_rf2 = mkt_rf(61:end,:); 

%Schleife welche sagt, f�r welche monate wir welches beta verwenden.
for n =1:200
    b  = beta(:,n);
    b2 = repmat(b,1,12)';
    b3 = reshape(b2,348,1);
    beta2(:,n) = b3;
end   

%--------------------------------------------------------------------------
% Construct other portfolio based on market cap.
%--------------------------------------------------------------------------
%I want to construct portfolios for stocks with ascending market
%capitalization.

cap_size = price.*shares;
cap_size = cap_size(61:end,:);

beta_size = ones(348,200);

%Loop that sorts each row with respect to market cap. and stores the
%corresponding beta in a matrix with the same index.
for i=1:348
    s = cap_size(i,:);
    s = [beta2(i,:);s];
    s = sortrows(s',2);
    s = s';
    beta_size(i,:) = s(1,:);
    cap_size(i,:) = s(2,:);
end

%Construct Portfolio (daily rebalancing)
[T_r T_c] = size(cap_size);
breakpoints = [1 T_c*0.2 T_c*0.4 T_c*0.6 T_c*0.8 T_c];
daily_portf = zeros(T_c,5);
daily_beta = zeros(T_c,5);
for k=1:T_c
    for j=1:5
        strt = round(breakpoints(j));
        nd = round(breakpoints(j+1));
        daily_portf(k,j)=mean(cap_size(k,strt:nd));
        daily_beta(k,j)=mean(beta_size(k,strt:nd));
    end
end

%Now I could extract the corner Portfolios but I have no idea how this
%tells me anything.