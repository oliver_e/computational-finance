clear; clc

% LECTURE 8

% -------------------------------------------------------------------------
load('usstocks200') % loads monthly returns of 200 us stocks

%------------------
% four assets
%------------------

E = mean(ret(:,1:4))';      % average returns
S = std(ret(:,1:4))';
V = cov(ret(:,1:4));        % variance-covariance matrix
w_naive = [1/4; 1/4; 1/4; 1/4];  % naive portfolio weights 

% equally weighted (naive) portfolio
EW_E = w_naive' * E;
EW_S = (w_naive' * V * w_naive)^0.5;
disp([EW_E EW_S])

scatter(S, E)
axis([0 0.15 0 0.025])
xlabel('risk');
ylabel('return'); 
hold on

scatter(EW_S, EW_E, 'gd');
text(EW_S, EW_E,'EW','horizontal','left', 'vertical','bottom')

% Alternative:
% P_ret = ret(:,1:3)*w_naive;
% disp([mean(P_ret) std(P_ret)]);

% -------------------------------------
% markowitz portfolio optimization 
% -------------------------------------

% fmincon fuction (http://www.mathworks.de/help/optim/ug/fmincon.html)
% x_out = fmincon(@(x) fun(x,Y),x0,A,b,Aeq,beq)

% fun: function to be minimized  (portfolio variance)
% x:   minimizer of the function (portfolio weights)
% A*x <= b      (inequality constraint, unused at the moment)<<shortsale
% constraint!
% Aeq*x = beq   (equality constraint, portfolio weights have to sum to one!)

% example: portfolio weights sum to one constraint
Aeq = [1 1 1 1];
beq = Aeq * w_naive;  
clear Aeq beq

% options
% to add options, write: 
% x_out = fmincon(@(x) fun(x,Y),x0,A,b,Aeq,beq,[],[],[],options)

options=[];
options=optimset(options,'LargeScale','off');
options=optimset(options,'TolFun',1e-15); % <- this option is important!
%Toleranz ist im Default zu gross
options=optimset(options,'Display','off');

% find portfolio with minimum variance
w0 =w_naive;    %Startwert
%Als Inputs Matrizen verwenden.
w=fmincon(@(w) w'*V*w,w0,[],[],[1 1 1 1],1,[],[],[],options);
w_minV = w;
disp(w_minV)

MIN_E = w_minV'*E;
MIN_S = (w_minV'*V*w_minV)^0.5;
disp([MIN_E MIN_S]);

scatter(MIN_S, MIN_E, 'rd','filled');
text(MIN_S, MIN_E,'MIN','horizontal','left', 'vertical','bottom')

% find portfolio with a return of 0.01 and smallest risk
% -> additional equality constraint

% E * w = 0.01
% Aeq2 = E;
% beq2 = 0.01;  

%Achtung: Aeq muss hier anders gesetzt werden! Gewichte m�ssen zu 1
%addieren.
w0 =w_naive; 
w=fmincon(@(w) w'*V*w,w0,[],[],[E'; 1 1 1 1],[0.01; 1],[],[],[],options);
w_1 = w;
disp(w_1)

P1_E = w_1'*E;
P1_S = (w_1'*V*w_1)^0.5;
disp([P1_E P1_S]);
scatter(P1_S, P1_E, 'r*');

% find portfolio with a return of [0.08; 0.01; 0.012; 0.014; 0.016; 0.018] and smallest risk

r_pf = [0.008; 0.01; 0.012; 0.014; 0.016; 0.018];
for i =1:6
    w0 =w_naive; 
    w=fmincon(@(w) w'*V*w,w0,[],[],[E'; 1 1 1 1],[r_pf(i,:); 1],[],[],[],options);
    P_E(i,:) = w'*E;
    P_S(i,:) = (w'*V*w)^0.5;
end    
disp([P_E P_S]);
scatter(P_S, P_E, 'r*'); %Plotte als roten Stern.

% compute the mean variance frontier
% repeat for a very fine grid
r_grid = [0:0.0001:0.022]';
for i =1:size(r_grid,1)
    w0 =w_naive; 
    w=fmincon(@(w) w'*V*w,w0,[],[],[E'; 1 1 1 1],[r_grid(i,:); 1],[],[],[],options);
    P_E(i,:) = w'*E;
    P_S(i,:) = (w'*V*w)^0.5;
end 
plot(P_S, P_E, 'r');

% find the portfolio with the maximum sharpe ratio (= tangent portfolio)

% sharpe ratio: (E-rf)/std;
% assume rf = 0.001 (=1.2% p.a.)
for i =1:size(r_grid,1)
    sharperatio(i,1)=(P_E(i,1)-0.001)/P_S(i,1);
end
i_tangency = find(sharperatio == max(sharperatio));

TP_E = P_E(i_tangency,:);
TP_S = P_S(i_tangency,:);
disp([TP_E MIN_S]);
scatter(TP_S, TP_E, 'rd','filled');
text(TP_S, TP_E,'TP','horizontal','left', 'vertical','bottom')

% draw the tangent
line([0,max(P_S)],[0.001,0.001+(TP_E-0.001)/TP_S*max(P_S)]) ;


% -------------------------------
% Optimize over all 200 stocks
%--------------------------------

% -> as long as there are no inequality constraints, we can compute the optimal
% portfolio weights analytically
% -> much faster than fmincon

% analytical solution:
% four assets
E = mean(ret)';      % average returns
S = std(ret)';
V = cov(ret);        % variance-covariance matrix

N = 200;
w_naive = ones(200,1)*1/N;  % naive portfolio weights 
O = ones(200,1);

% plot stocks and 
fig2 = figure;
scatter(S, E)
axis([0 0.15 0 0.04])
xlabel('risk');
ylabel('return'); 
hold on

% EW portfolio
EW_E = w_naive' * E;
EW_S = (w_naive' * V * w_naive)^0.5;

scatter(EW_S, EW_E, 'gd');
text(EW_S, EW_E,'EW','horizontal','left', 'vertical','bottom')

% minimum variance portfolio
w_MIN = (V\O)/(O'/V*O);
MIN_E= w_MIN'*E;
MIN_S=(w_MIN'*V*w_MIN)^0.5;
scatter(MIN_S, MIN_E, 'rd','filled');
text(MIN_S, MIN_E,'MIN','horizontal','left', 'vertical','bottom')

% tangential portfolio
w_TP = (V\(E-0.001))/(O'/V*(E-0.001));
TP_E= w_TP'*E;
TP_S=(w_TP'*V*w_TP)^0.5;
scatter(TP_S, TP_E, 'rd','filled');
text(TP_S, TP_E,'TP','horizontal','left', 'vertical','bottom')

% add a mean-variance frontier to the figure
A=E'/V*E; B=E'/V*O; C=O'/V*O; % parameters
r_grid = [0:0.0001:0.04]';
for i=1:size(r_grid);
    w=V\( (E*(C*r_grid(i)-B)+O*(A-B*r_grid(i))) / (A*C-B^2) );
    P_E(i,1)= w'*E;
    P_S(i,1)=(w'*V*w)^0.5;
end    
plot(P_S,P_E,'r'); 

% draw the tangent
line([0,max(P_S)],[0.001,0.001+(TP_E-0.001)/TP_S*max(P_S)]) ;

% take a closer look at the weights of the tangency portfolio
% are they realistic?
bar(w_TP)

% No, very unrealistic. Too manny stocks have a negative optimal portfolio
% weight. 

% -> Problem of estimation uncertainty



