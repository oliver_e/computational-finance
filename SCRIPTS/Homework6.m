% Oliver Engist
% 2012-050-225
% Homework 6
%--------------------------------------------------------------------------

clear; clc
%--------------------------------------------------------------------------
%Preparation
%--------------------------------------------------------------------------

%Load and read Microsoft Data:
[MSFT_data MSFT_txt]=xlsread('NASDAQ_MSFT.xls');

MSFT_close = MSFT_data(:,5);

%Save as a matrix
save('MSFT_close');

%Calculate Returns
R_msft = calcReturn(MSFT_close)-1;
[T_r T_c]= size(R_msft);

%--------------------------------------------------------------------------
%We only want to estimate GARCH on the first 2/3 of the returns.
[T_r T_c] = size(R_msft);
index = floor(2*T_r/3);
R_msft23 = R_msft(1:index);
R_msft33 = R_msft(index+1:end);
mu_msft = mean(R_msft);
sigma_msft = std(R_msft);
%Estimate a, b and w of the MSFT data using MFE toolbox
param = tarch(R_msft23,1,0,1);

w = param(1);
a = param(2);
b = param(3);

%Estimate volatility of following day
vola = zeros(T_r-index,1);
VaRest = zeros(T_r-index,1);
VaRest(1) = prctile(R_msft23,5);

for i=2:(T_r-index)
    vola(i) = sqrt(w+a*R_msft33(i)^2+b*vola(i-1)^2);
    VaRest(i) = norminv(0.05,mu_msft,vola(i));
end

%Compare with the VaR of the sample:
VaR = prctile(R_msft23,5);
%Difference to the constant VaR:
VaRdiff_c = (R_msft23<VaR);
%Do more than 5% of returns exceed VaR?
perc_const = sum(VaRdiff_c)/T_r;

[T_rG T_cG]=size(R_msft33);
%Difference to the GARCH VaR:
VaRdiff_g = (R_msft33<VaRest);
%Do more than 5% of returns exceed GARCH VaR?
perc_garch = sum(VaRdiff_g)/T_rG;


subplot(3,1,1);
plot(vola);
xlabel('GARCH estimated vola');
subplot(3,1,2);
plot(VaRest);
xlabel('Estimated VaR');
hold on;
line([0 T_rG],[VaR VaR],'Color',[1,0,0]);
hold off;

%--------------------------------------------------------------------------
%Bonus:
%--------------------------------------------------------------------------
vola2 = zeros(T_rG,1);

for i=2:T_rG
    vola2(i) = sqrt(w+a*R_msft33(i)^2+b*vola(i-1)^2);
    VaRest2(i) = norminv(0.05,mu_msft,vola2(i));
    [text param] = evalc('tarch(R_msft(1:index+i-1),1,0,1)');
    w = param(1);
    a = param(2);
    b = param(3);
end

subplot(3,1,3);
plot(vola2);

VaRdiff_g2 = (R_msft33'<VaRest2);
perc_garch2 = sum(VaRdiff_g2)/T_rG

%Model doesn't improve. 2.7% instead of the 5%...even worse than the
%constant VaR.

%Rolling window as an alternative time window.