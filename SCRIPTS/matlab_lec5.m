clear; clc

% LECTURE 5

% ----> write function simsec.m (homework)

%--------------------------------------------------------------------------
% simulation - simple

load('MSFT');
sigma=std(R_msft); mu=mean(R_msft)-.5*sigma^2; T=length(R_msft); 
R_msft_sim=simsec(mu,sigma,T);
 
disp([mean(R_msft) mean(R_msft_sim)]);
disp([std(R_msft) std(R_msft_sim)]);
disp([skewness(R_msft) skewness(R_msft_sim)]);
disp([kurtosis(R_msft) kurtosis(R_msft_sim)]);
 
subplot(2,1,1); hist(R_msft,[-.2:.01:.2]); title('Actual'); axis([-.2 .2 0 1200]);
subplot(2,1,2); hist(R_msft_sim,[-.2:.01:.2]); title('Simulated'); axis([-.2 .2 0 1200]);

% Compare simulated to actual data

%--------------------------------------------------------------------------
% simulation with "jumps"

% ----> write function simsecJ.m

% calibrate to MSFT
nlow=sum(R_msft<-.08);
nhigh=sum(R_msft>.08);
[T a]=size(R_msft);
p1=nlow/T; p2=nhigh/T; %Naive Berechnung der Wahrscheinlichkeit von starken Ausreissern
J1=-.13; J2=.13;        %Der Wert +-.13 als Return bei einem Jump ist grob gesch�tzt.

[R_msft_simJ J]=simsecJ(mu,sigma,J1,J2,p1,p2,T);

disp([mean(R_msft) mean(R_msft_simJ)]);
disp([std(R_msft) std(R_msft_simJ)]);
disp([skewness(R_msft) skewness(R_msft_simJ)]);
disp([kurtosis(R_msft) kurtosis(R_msft_simJ)]);

subplot(2,1,2); hist(R_msft_simJ,[-.2:.01:.2]); title('Simulated with Jumps'); axis([-.2 .2 0 1200]);

% note that kurtosis of simulated now matches actual

%--------------------------------------------------------------------------
% stochastic volatility

% volatility is predictable
T1=floor(T/20); ma=zeros(T1,1);
for i=1:T1;
    % 20 day standard deviation
    in=(i-1)*20+1:(i-1)*20+20;
    ma(i,1)=std(R_msft(in,1));
end;
white_noise=exp(randn(T1,1)); 

subplot(2,1,1); plot(ma(:,1)); title('actual 7 day standard deviation');
subplot(2,1,2); plot(white_noise); title('simulated exp(white noise)');

% predictability regressions
% white noise - benchmark
X=[ones(T1-1,1) white_noise(1:T1-1,1)];
Y=white_noise(2:T1,1);

[regcoef sterr c d rsq]=regress(Y,X);
disp([regcoef sterr]);
disp(rsq(1));

% actual data
X=[ones(T1-1,1) ma(1:T1-1,1)];
Y=ma(2:T1,1);
[regcoef sterr c d rsq]=regress(Y,X);
disp([regcoef sterr]);
disp(rsq(1));

% scatter plot
subplot(1,2,1); plot(ma(1:T1-1,1),ma(2:T1,1),'.'); title('actual 20 day standard deviation');
subplot(1,2,2); plot(white_noise(1:T1-1,1),white_noise(2:T1,1),'.');  title('simulated exp(white noise)');

% simulation witch stochastic volatility
sigmaS=0.0035; rho=0.99; muS=0.002;
[R_msft_simSV]=simsecSV(mu,muS,J1,J2,p1,p2,rho,sigmaS,T);

%Berechnen die std von 20 Tag Intervallen. Keine �berlappung.
T1=floor(T/20); masim=zeros(T1,1);
 for i=1:T1;
    in=(i-1)*20+1:(i-1)*20+20;
    masim(i,1)=std(R_msft_simSV(in,1));
end;

% predictability regressions, SV
%Regression der Volatilit�t auf die des Vortages (y wird zu x). 
%Volatilit�ten kommen aus der Berechnung f�r 20d Intervalle.
X=[ones(T1-1,1) masim(1:T1-1,1)]; Y=masim(2:T1,1);
[regcoef sterr c d rsq]=regress(Y,X);
disp([regcoef sterr]); disp(rsq(1));

disp([mean(R_msft) mean(R_msft_simSV)]);
disp([std(R_msft) std(R_msft_simSV)]);
disp([skewness(R_msft) skewness(R_msft_simSV)]);
disp([kurtosis(R_msft) kurtosis(R_msft_simSV)]);

subplot(2,1,1); plot(ma(:,1)); title('actual - 20 day vol');
subplot(2,1,2); plot(masim(:,1)); title('simulated - 20 day vol');

