clear; clc

% LECTURE 9

% -------------------------------------------------------------------------
% Compute BETA sorted portfolios
% -------------------------------------------------------------------------

clear; clc
load('usstocks200')% loads monthly returns of 200 us stocks
load('rf') % loads the monthly risk-free rate

mkt = mean(ret')'; %equally weighted portfolio as proxy for the market return

ret_rf = ret-repmat(rf,1,200);  % excess returns
mkt_rf = mkt-rf;                % excess market return

% compute rolling betas

% example: first beta
for n=1:200
    [b,bint,r,rint,stats] = regress(ret_rf(1:60,n),[ones(60,1) mkt_rf(1:60,:)]);
    beta(:,n) = b(2,1);
end

% rolling annual betas
%Immer Dezember des Jahres

A = [60:12:396];
for t=1:size(A,2)
    % first beta
    T = A(t);
    for n=1:200 %200 stocks
        [b] = regress(ret_rf(T-60+1:T,n),[ones(60,1) mkt_rf(T-60+1:T,:)]);
        beta(t,n) = b(2,1);
    end
end

plot(beta(:,25)) % betas of individual stocks can be quite unstable over time

%wenn wir portfolios mit spezifischen betas konstruieren w�rde eine aktie
%mit unkonstantem beta �ber die zeit zwischen den portfolios wandern.

% match december betas with the returns in the following year
ret_rf2 = ret_rf(61:end,:); %die daten zur initialen beta berechnung m�ssen wir rausschneiden
mkt_rf2 = mkt_rf(61:end,:); 

%Schleife welche sagt, f�r welche monate wir welches beta verwenden.
for n =1:200
    b  = beta(:,n);
    b2 = repmat(b,1,12)';
    b3 = reshape(b2,348,1);
    beta2(:,n) = b3;
end   

% allocate stocks according to their beta into 10 decile portfolios
% stocks in pofo one have the lowest beta, etc.

% 1) compute break points
break_points = quantile(beta2,[1/10 2/10 3/10 4/10 5/10 6/10 7/10 8/10 9/10],2);

%Die quantile sind wichtig, weil wir danach dann die portfolios
%konstruieren. alle betas die kleiner als das entsprechende quantil sind
%kommen in dieses portfolio. wir haben dann ein 10%, 20%, 30% ...
%portfolio. nun haben wir portfolios mit konstanten betas.
%Wichtig: man muss das nat�rlich f�r jedes jahr neu berechnen.

% 2) compute portfolio returns, allocate stocks in 10 portfolio according
% their beta and the break points
for t = 1:348
    [c,idx] = histc(beta2(t,:).',[-inf break_points(t,:) inf]); % allocates stocks in 10 portfolios in idx, c is the number of stocks in each portfolio 
    pf_rf(t,:) = accumarray(idx,ret_rf2(t,:),[],@mean);         % compute average return for each portfolio
end

% check portfolio sort
disp([mean(pf_rf)*1200; std(pf_rf)*(12^0.5)*100]);

% -------------------------------------------------------------------------
% TESTING THE CAPM %
% -------------------------------------------------------------------------

% According to the CAPM, the market portfolio is the ex ante tangent
% portfolio, it is "efficient". As a result all assets (and portfolios) should lie on the
% security market line (SML), which can be written formally as

% E(RET_{i}-RF) = 0 + BETA_{i} * E(MKT_{i}-RF) 

% Regression Model to test the SML:
% RET_{t;i}-RF = alpha + BETA_{i} * (MKT_{t;i}-RF) + e_{t;i}

for n =1:10
[b] = regress(pf_rf(:,n),[ones(348,1) mkt_rf2]);
beta_pf(1,n)=b(2,:);
end  

[b] = regress(mkt_rf2,[ones(348,1) mkt_rf2]);
beta_mkt = [b(2,:)];

scatter(beta_pf, mean(pf_rf)); hold on
scatter(beta_mkt, mean(mkt_rf2), 'r*');
line([0,max(beta_pf)],[0,mean(mkt_rf2)/1*max(beta_pf)]) ;
xlabel('beta'); % x-axis lable
ylabel('mean excess return'); % y-axis lable
title('SML for 10 Portfolios Sorted by Beta'); % title of the plot

% we want to estimate the alphas, the part of the return that is not
% explained by the beta

fit_capm =  fitlm(mkt_rf2,pf_rf(:,1)); % the problem with fitlm is that is does not provide s.e. as an output
disp(fit_capm.Coefficients)


for n =1:10
    fit_capm =  fitlm(mkt_rf2,pf_rf(:,n));
    alpha(:,n) = table2array(fit_capm.Coefficients(1,1));
    alpha_se(:,n) = table2array(fit_capm.Coefficients(1,2));
    alpha_t(:,n) = table2array(fit_capm.Coefficients(1,3));
end 
disp(alpha*100)
disp(alpha_t)

% low beta stocks are underpriced, high beta stocks are overpriced!
% However, is this under-/over pricing significant?

% To statistically evaluate the CAPM we want to test whether the alphas are jointly diffent from zero.  
% this can be accomplished by the test statistic (alpha'*[Var(alpha)]^(-1)*alpha), a Wald test statistic 

% what is missing now is is Var(alpha)...

% It turns out that: [Var(alpha)] = S * (1+(mu_{mkt}/cov_{mkt}*mu_{mkt})
% where S is the variance covariance matrix of the CAPM regression residuals

T = 348;
e = pf_rf - [ones(348,1) mkt_rf2]*[alpha' beta_pf']'; % CAPM regressions residuals
S=(1/(T-1))*(e')*e; % variance covariance matrix of the CAPM regression 
Var_alpha = S*(1+(mean(mkt_rf2)/cov(mkt_rf2)*mean(mkt_rf2)));

% Wald Test
Wald_test = T*(alpha/Var_alpha*alpha'); 
Wald_test_pv=1-chi2cdf(Wald_test,10); % The Wald test statistic is chi^2 distributed
disp([Wald_test; Wald_test_pv])

% -------------------------------------------------------------------------
% A Mean-Variance Interpretation  %
% -------------------------------------------------------------------------

% compute and plot the mean-variance frontier of ten beta sorted portfolios
E=mean(pf_rf)'; V=cov(pf_rf); O=ones(10,1);
A=E'/V*E; B=E'/V*O; C=O'/V*O;

r_grid = [0:0.0001:0.03]';
for i=1:size(r_grid);
    w=V\( (E*(C*r_grid(i)-B)+O*(A-B*r_grid(i))) / (A*C-B^2) );
    P_E(i,1)= w'*E;
    P_S(i,1)=(w'*V*w)^0.5;
end    
plot(P_S,P_E,'r'); hold on

% locate the beta sorted portfolios
scatter(std(pf_rf), mean(pf_rf))

% locate the ex post tangent portfolio
w_tp=(V\E)/(O'/V*E); % calculate the weights for the tangency portfolios
TP=pf_rf*w_tp;
scatter(std(TP), mean(TP), 'rd','filled')
line([0,max(P_S)],[0,0+(mean(TP)-0)/std(TP)*max(P_S)]) ;

% locate the market portfolio
scatter(std(mkt_rf), mean(mkt_rf), 'gd','filled')

% the ex post TP prices the 10 beta portfolios correctly, 
% it is "efficient" % by definition
for n =1:10
    fit_capm =  fitlm(TP,pf_rf(:,n));
    alpha(:,n) = table2array(fit_capm.Coefficients(1,1));
    alpha_se(:,n) = table2array(fit_capm.Coefficients(1,2));
    alpha_t(:,n) = table2array(fit_capm.Coefficients(1,3));
end 
disp(alpha*1200)

% Put differently, alphas, the slope of the ex post tangent portfolio (= Sharpe
% Ratio of the tangent) and the slope of the market portfolio (= Sharpe
% Ratio of the market portfolio) must be related to each other.

% Gibons, Ross, Shanken (1989) show in a famous paper that
Wald_test2= T * ( mean(TP)^2/var(TP) - mean(mkt_rf2)^2/var(mkt_rf2) ) ./ ( 1+(mean(mkt_rf2)^2)/cov(mkt_rf2) );
disp([Wald_test; Wald_test2]);

    % Remark: More generally, you have to include the market portoflio when
    % computing TP. Here, the market portfolio is a linear combination of the
    % beta portfolios and we can skip it. Compute [mean(pf_rf')' mkt_rf2] to see
    % this

% To sum up: 

% In any finite sample, the market portfolio will be inside the ex post mean-variance frontier 
% To test the CAPM, we check by how much (alphas / Wald test, GRS test). 





