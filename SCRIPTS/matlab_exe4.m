% Exercise 4

clear; clc; % cleear the workspace and command window

%----
% 1 �
%--------------------------------------------------------------------------
% Create a function that will simulate security data using a log-normal process	
% The function should take in arguments mu (mean), sigma (standard deviation), and T (number of days)
% Its output should be a vector of daily returns

% ---> see function simsec.m

%----
% 2 �
%--------------------------------------------------------------------------
% Use this function to simulate coca-cola data
% (for a clean dataset, see NYSE_KO.xls)

[data data_txt] = xlsread('NYSE_KO.xls'); % LOAD DATA

P_ko = data(:,4); 
R_ko = P_ko(2:end,:)./P_ko(1:end-1,:)-1; % simple return
r_ko = log(P_ko(2:end,:))-log(P_ko(1:end-1,:)); % log return

date_txt_ko = data_txt(2:end,1);
formatIn = 'dd.mmm.yyyy';
date_num_ko = datenum(date_txt_ko, formatIn);

clear data_txt data formatIn
save('KO')

% simple returns
stats_R_ko = [mean(R_ko)*100; std(R_ko)*100; min(R_ko)*100; max(R_ko)*100; skewness(R_ko); kurtosis(R_ko)];
disp(stats_R_ko)

% log returns
stats_r_ko = [mean(r_ko)*100; std(r_ko)*100; min(r_ko)*100; max(r_ko)*100; skewness(r_ko); kurtosis(r_ko)];
disp(stats_r_ko)

% remeber that mean(R_ko) =~ mean(r_ko) + 0.5*std(r_ko)^2
disp([mean(R_ko); mean(r_ko); mean(r_ko)+0.5*std(r_ko)^2])

% Check if functions works properly
T=100000; 
sigma=std(R_ko);
mu=mean(R_ko)-0.5*std(R_ko)^2; 

R_ko_sim = simsec(mu,sigma,T);

disp([mean(R_ko) mean(R_ko_sim)]*100);
disp([std(R_ko) std(R_ko_sim)] *100);

% => YES!

% Now simulate daily coca cola data
T=size(R_ko,1); 
sigma=std(R_ko);
mu=mean(R_ko)-0.5*std(R_ko)^2; 

R_ko_sim = simsec(mu,sigma,T);

% compare hitsorical with simulated returns
disp([mean(R_ko) mean(R_ko_sim)]*100);
disp([std(R_ko) std(R_ko_sim)] *100);

subplot(2,1,1); 
hist(R_ko,[-.2:.01:.2]);
axis([-.2 .2 0 1200]);
xlabel('Historical');
subplot(2,1,2);
hist(R_ko_sim,[-.2:.01:.2]); 
axis([-.2 .2 0 1200]);
xlabel('Simulated');


% Q: Why T=100000?
% A: Return standard errors are very big! It is unlikley to get
% mean(R_sim) close to mean(R) for low Ts. To check that the simsec.m
% simulates the mean properly we need to simulate a huge number (T) of returns

% s.e.(R)= std(R)/sqrt(T)

% T = 3269
% s.e.(R_ko)) = std(R_ko) / sqrt(size(R_ko,1)) = 0.0223% 
% mean(R_ko) = 0.017%
% => huge 95% confidence interval: [0.017-2*0.0223, 0.017+2*0.0223];

% T = 100000
% s.e.(R_ko)) = std(R_ko) / sqrt(100000) = 0.0040
% mean(R_ko) = 0.017%
% => small 95% confidence interval: [0.017-2*0.004, 0.017+2*0.004];

