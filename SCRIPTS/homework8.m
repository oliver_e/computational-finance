%Homework 8
%Oliver Engist
%--------------------------------------------------------------------------
%Efficiency Frontier without shortsale constraint:
%--------------------------------------------------------------------------
clear; clc;
load('usstocks200');

E = mean(ret(:,1:4))';      % average returns
S = std(ret(:,1:4))';       % stand. dev.
V = cov(ret(:,1:4));        % variance-covariance matrix

% Equally weighted portfolio
w_naive = [1/4; 1/4; 1/4; 1/4];

EW_E = w_naive' * E;
EW_S = (w_naive' * V * w_naive)^0.5;
disp([EW_E EW_S])


scatter(S, E)
axis([0 0.15 0 0.025])
xlabel('risk');
ylabel('return'); 
hold on

scatter(EW_S, EW_E, 'gd');
text(EW_S, EW_E,'EW','horizontal','left', 'vertical','bottom')

%Set Options of fmincon():
options=[];
options=optimset(options,'LargeScale','off');
options=optimset(options,'TolFun',1e-15); % <- this option is important!
%Toleranz ist im Default zu gross
options=optimset(options,'Display','off');

% find portfolio with minimum variance
w0 =w_naive;    %Startwert
%Als Inputs Matrizen verwenden.
w=fmincon(@(w) w'*V*w,w0,[],[],[1 1 1 1],1,[],[],[],options);
w_minV = w;
disp(w_minV)

MIN_E = w_minV'*E;
MIN_S = (w_minV'*V*w_minV)^0.5;
disp([MIN_E MIN_S]);

scatter(MIN_S, MIN_E, 'rd','filled');
text(MIN_S, MIN_E,'MIN','horizontal','left', 'vertical','bottom')

% find portfolio with a return of 0.01 and smallest risk
% -> additional equality constraint

% E * w = 0.01
% Aeq2 = E;
% beq2 = 0.01;  

%Achtung: Aeq muss hier anders gesetzt werden! Gewichte m�ssen zu 1
%addieren.
w0 =w_naive; 
w=fmincon(@(w) w'*V*w,w0,[],[],[E'; 1 1 1 1],[0.01; 1],[],[],[],options);
w_1 = w;
disp(w_1)

P1_E = w_1'*E;
P1_S = (w_1'*V*w_1)^0.5;
disp([P1_E P1_S]);
scatter(P1_S, P1_E, 'r*');

% find portfolio with a return of [0.08; 0.01; 0.012; 0.014; 0.016; 0.018] and smallest risk

r_pf = [0.008; 0.01; 0.012; 0.014; 0.016; 0.018];
for i =1:6
    w0 =w_naive; 
    w=fmincon(@(w) w'*V*w,w0,[],[],[E'; 1 1 1 1],[r_pf(i,:); 1],[],[],[],options);
    P_E(i,:) = w'*E;
    P_S(i,:) = (w'*V*w)^0.5;
end    
disp([P_E P_S]);
scatter(P_S, P_E, 'r*'); %Plotte als roten Stern.

% compute the mean variance frontier
% repeat for a very fine grid
r_grid = [0:0.0001:0.022]';
for i =1:size(r_grid,1)
    w0 =w_naive; 
    w=fmincon(@(w) w'*V*w,w0,[],[],[E'; 1 1 1 1],[r_grid(i,:); 1],[],[],[],options);
    P_E(i,:) = w'*E;
    P_S(i,:) = (w'*V*w)^0.5;
end 
plot(P_S, P_E, 'r');

% find the portfolio with the maximum sharpe ratio (= tangent portfolio)

% sharpe ratio: (E-rf)/std;
% assume rf = 0.001 (=1.2% p.a.)
for i =1:size(r_grid,1)
    sharperatio(i,1)=(P_E(i,1)-0.001)/P_S(i,1);
end
i_tangency = find(sharperatio == max(sharperatio));

TP_E = P_E(i_tangency,:);
TP_S = P_S(i_tangency,:);
disp([TP_E MIN_S]);
scatter(TP_S, TP_E, 'rd','filled');
text(TP_S, TP_E,'TP','horizontal','left', 'vertical','bottom')

% draw the tangent
line([0,max(P_S)],[0.001,0.001+(TP_E-0.001)/TP_S*max(P_S)]) ;

%--------------------------------------------------------------------------
% Shortsale constraint
%--------------------------------------------------------------------------
warning('off');
% use r_grid as stepsize
r_grid = [0:0.0001:0.022]';
% do the same loop again with constraints...
A = diag(ones(1,4),0);
z = zeros(4,1);
tic;
for i =1:size(r_grid,1)
    w0 =w_naive; 
    w=fmincon(@(w) w'*V*w,w0,-A,z,[E'; 1 1 1 1],[r_grid(i,:); 1],[],[],[],options);
    P_E_con(i,:) = w'*E;
    P_S_con(i,:) = (w'*V*w)^0.5;
end 
toc;
plot(P_S_con, P_E_con, 'g');

%Find tangency point
for i =1:size(r_grid,1)
    sharperatio_con(i,1)=(P_E_con(i,1)-0.001)/P_S_con(i,1);
end

tang_con = find(sharperatio_con == max(sharperatio_con));

TP_E_con = P_E_con(tang_con,:);
TP_S_con = P_S_con(tang_con,:);
scatter(TP_S_con, TP_E_con, 'rd','filled');
text(TP_S_con, TP_E_con,'TP','horizontal','left', 'vertical','bottom')

line([0,max(P_S_con)],[0.001,0.001+(TP_E_con-0.001)/TP_S_con*max(P_S_con)]) ;

%--------------------------------------------------------------------------
% With 100 stocks:
%--------------------------------------------------------------------------
E = mean(ret(:,1:100))';      % average returns
S = std(ret(:,1:100))';       % stand. dev.
V = cov(ret(:,1:100));        % variance-covariance matrix

% Equally weighted portfolio
w_naive = ones(100,1)./4;

EW_E = w_naive' * E;
EW_S = (w_naive' * V * w_naive)^0.5;
disp([EW_E EW_S])

warning('off');
% use r_grid as stepsize
r_grid = [0:0.0001:0.022]';
% do the same loop again with constraints...
A = diag(ones(1,100),0);
z = zeros(100,1);
tic;

for i =1:size(r_grid,1)
    w0 =w_naive; 
    w_100=fmincon(@(w) w'*V*w,w0,-A,z,[E'; A],[r_grid(i,:); 1],[],[],[],options);
    P_E_con(i,:) = w'*E;
    P_S_con(i,:) = (w'*V*w)^0.5;
end 
toc;
plot(P_S_con, P_E_con, 'g');

%Find tangency point
for i =1:size(r_grid,1)
    sharperatio_con(i,1)=(P_E_con(i,1)-0.001)/P_S_con(i,1);
end

tang_con = find(sharperatio_con == max(sharperatio_con));

TP_E_con = P_E_con(tang_con,:);
TP_S_con = P_S_con(tang_con,:);
scatter(TP_S_con, TP_E_con, 'rd','filled');
text(TP_S_con, TP_E_con,'TP','horizontal','left', 'vertical','bottom')

line([0,max(P_S_con)],[0.001,0.001+(TP_E_con-0.001)/TP_S_con*max(P_S_con)]) ;

%--------------------------------------------------------------------------
%Market Portfolio
%--------------------------------------------------------------------------

%Use number of shares as weight:
[T_r T_c] = size(shares);
w_m = shares(T_r,:)./sum(shares(T_r,:));

E_m = mean(ret);
S_m = std(ret);
V_m = cov(ret);

MW_E = w_m*E_m';
MW_S = (w_m*V_m*w_m').^.5;

%Draw market Portfolio as a blue star in the scatter plot.
scatter(MW_S, MW_E,'b*');
hold off;