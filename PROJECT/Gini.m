function [ area ] = Gini( r_array )
%GINI Calculates the area between Lorenz curve and linear line.
%   Takes return array as input. 
%   Uses external function lorenz().
%This is a very simple approach, but it is closest to the computed Ginis
%from the paper!

[lor rf] = lorenz(r_array);
[T_r T_c] = size(lor);
if T_r>T_c
    size_R=T_r;
else
    size_R = T_c;
end
area = 0;
for i=1:size_R
    d = rf(i)-lor(i);
    a = d*1/size_R;
    area = area+a;
end
area = area*2;
end

