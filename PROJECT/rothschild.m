function [ x ] = rothschild( cum1, cum2 )
%ROTHSCHILD just walks along a lorenz curve and checks for rothschild
%stiglitz condition.
%   takes two lorenz functions as input and checks whether they cross at
%   any point. returns 1 if the first cumulative sum is always greater,
%   returns 0 if they cross and -1 if the second is always greater.
% DO NOT USE THIS FUNCTION FROM OUTSIDE. ITS ONLY PURPOSE IS TO CALCULATE
% R&S CONDITION IN DOMINATE!
[T_r T_c]=size(cum1);
j=2;
if sum(find(cum1(j:end)>cum2(j:end)))==T_r
    x = 1;
elseif sum(find(cum1(j:end)<cum2(j:end)))==T_r
    x = -1;
else
    x = 0;
      
end

