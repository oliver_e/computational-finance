function r=simsec2(mu,sigma,T)
% simulate security returns

x=randn(T,1); 
mu_cal = mu-0.5*(sigma^2);

for t=1:T;
    r(t,1)=exp(mu_cal+sigma*x(t))-1;
end;

% important: mu is the mean of the log return!
% this function simulates log returns which are then transformed to simple returns
% we do this to avoid impossible simulated returns which are lower than -100% 

% 1+r       =   exp( mu + sigma * x(t) )
% log(1+r)  =   mu + sigma * x(t)