function [r J] = simsecJ2(mu,sigma,J1,J2,p1,p2,T)
mu = mu-0.5*sigma^2;
x=randn(T,1); y=rand(T,1);
r = zeros(T,1);
J = zeros(T,1);
for t=1:T
    if y(t)<p1
       J(t)=J1;
    elseif y(t)<p1+p2
       J(t)=J2;
    else
       J(t)=0;
    end
end
r=exp(mu+sigma*x+J)-1;
end

