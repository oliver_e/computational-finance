function [ R ] = calcReturn( prices )
%CALCRETURN takes an array of n (closing) prices as input.
%Returns return array of length n-1
R = (prices(2:end)./prices(1:end-1));
end

