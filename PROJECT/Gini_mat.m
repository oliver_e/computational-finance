function [ area1 ] = Gini_mat( values )
%GINI_MAT calculates Gini Coefficient with Rieman-Sums.

[lor rf] = lorenz(r_array);
[T_r T_c] = size(lor);
if T_r>T_c
    size_R=T_r;
else
    size_R = T_c;
end
area1 = 0;
for i=1:size_R
    d1 = rf(i)-lor(i);
    a1 = d1*1/size_R;
    area1 = area1+a1;
    
    
end



end

