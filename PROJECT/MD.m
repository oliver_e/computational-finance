function [ MD ] = MD( input )
%RMD calculates the relative mean difference (Gini's MD) for a given input
%array. The MD/2 equals the Gini Coefficient (or Gamma if we want to use it
%for test of stochastic Dominance)
%   Takes array as input and returns scalar.
[T_r T_c] = size(input);
if T_r > T_c
    input_size = T_r;
else
    input_size = T_c;
end
total_diff = 0;
for i=1:input_size
    y = input(i);
    diff = 0;
    for j=1:input_size
        diff = diff + abs(y - input(j));
    end
    total_diff = total_diff+diff;
end

MD = total_diff/((input_size-1)*input_size);
RelMD = MD/sum(input);

end

