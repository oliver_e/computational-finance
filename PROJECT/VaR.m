function [ quant ] = VaR( ret_array, p )
%VAR Returns value at risk for demanded quantile
%   Takes return array and probability level as input.
[T_r T_c]=size(ret_array);
if T_r>T_c
    array_size = T_r;
else
    array_size = T_c;
end
idx = uint16(p*array_size);
if idx < 1
    idx = 1;
end
r_sort = sort(ret_array);
quant = r_sort(idx);
end

