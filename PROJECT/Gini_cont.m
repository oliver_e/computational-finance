%GINI_CONT computes a more accurate area between the Lorenz and the line
%for total equality. Instead of just taking the difference of the two
%curves and multiplying it by 1/sample-size, I interpolate the Lorenz
%data points to approach a continous function.

function [ area ] = Gini_cont( r_array )
[lor rf] = lorenz(r_array);
[T_r T_c] = size(lor);
if T_r>T_c
    size_R=T_r;
else
    size_R = T_c;
end

%Interpolate the Lorenz curve to get the points between the actual
%observations.
acc = 100;
lor_interp = zeros(size_R*acc,1);
for i=1:size_R-1
    x1 = lor(i);
    x2 = lor(i+1);
    lor_interp((i-1)*acc+1)=x1;
    for j=2:acc
        idx = (i-1)*acc+j;
        point = (x1*((acc+1)-j)+x2*(j-1))/acc;
        lor_interp(idx)=point;
    end
end

[T_r_lor T_c_lor] = size(lor_interp);
rf_line = [0:T_r_lor]*rf(1);

%Use the interpolated (smoothed) line to calculate the area between RF-line
%and Lorenz curve.
area = 0;
for i=1:T_r_lor
    d = rf_line(i)-lor_interp(i);
    a = d*1/T_r_lor;
    area = area+a;
end
area = area*2;

end

