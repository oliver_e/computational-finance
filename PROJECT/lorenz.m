function [cum rf_cum ] = lorenz( data )
%LORENZ Takes data sample as input, returns Lorenz and matrix containing
%cumulative product.
%   Detailed explanation goes here
[T_c T_r] = size(data);
if T_c > T_r
    length = T_c;
else 
    length = T_r;
end
data_sort = sort(data);
w = 1/length;

curve = zeros(length);
for i=1:length
    curve(i,i:length) = w;
end
cum = data_sort'*curve;
cum = [0,cum];
%calculate Lorenz (area). Therefore we need the riskless line.
ones(length,1);
%Last entry in cumprod equals mean.
mu = mean(data_sort);
x = ones(length,1)*mu;
rf_cum = x'*curve;
rf_cum = [0,rf_cum];
end

