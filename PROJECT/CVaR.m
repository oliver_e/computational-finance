function [ cvar ] = CVaR( r_array, percent)
%CVAR Takes Return array and percentile as input, returns CVaR.
%   This function uses external function lorenz to calculate CVaR.
[lor riskfree] = lorenz(r_array);

index = round(length(lor)*percent);
L_p = lor(index);
cvar = (L_p/percent);
end

