%Abschlussprojekt Computational finance
%Oliver Engist
%2012-050-225
%--------------------------------------------%
%Topic: Risk management using Lorenz curves  %
%--------------------------------------------%
clear; clc;

%Example of Lorenz curve for income inequality:
%The data are drawn from SHP dataset. Real data on net income of swiss
%fulltime employed.

income = xlsread('netincome.xls');
[inc_lor inc_rf] = lorenz(income);

plot(inc_lor);
hold on;
plot(inc_rf);
xlabel('Cumulative Population');
ylabel('Cumulative income');
axis([0 1 0 1]);
axis tight;
hold off;

%<-----------Breakpoint here
%Use simulated data to show behaviour of the Lorenz curve

sim = simsec2(0.05,0.2,1000);
sim_j = simsecJ2(0.05,0.2,.1,-.1,0.02,0.02,1000);
%sim_sv = simsecSV2(0.05,0.2,1000);

[lor_sim rf_sim] = lorenz(sim);
[lor_sim_j rf_sim_j] = lorenz(sim_j);
cvar_sim = CVaR(sim,0.95);
cvar_simJ = CVaR(sim_j, 0.95);


plot(lor_sim,'r');
hold on;
plot(rf_sim,'r');
plot(rf_sim_j,'g');
plot(lor_sim_j,'g');
line([0 length(sim)],[0 cvar_sim]);
line([0 length(sim_j)],[0 cvar_simJ]);
line([0 1000],[0 0])
xlabel('Cumulative trading days');
ylabel('Cumulative weighted returns')

axis tight;

%Draw a vertical line to show intersection of lorenz and CVaR line.
if mean(sim)>mean(sim_j)
    y_limit = cvar_sim*0.95;
else
    y_limit = cvar_simJ*0.95;
end
line([950 950],[0 y_limit], 'LineStyle','--');


%Show VaR in Graph as slope of the lorenz in specific alpha
% For simplicity, use 55% level. Makes nicer graph...
[T_r T_c] = size(sim);
[T_rj T_cj] = size(sim_j);
index = T_r*0.55;
index_j = T_rj*0.55;

simsort = sort(sim);
simsort_j = sort(sim_j);

VaR = simsort(index);
VaR_j = simsort_j(index_j);

a = lor_sim(550)-VaR*0.55;
a_j = lor_sim_j(550)-VaR_j*0.55;

w = [1:1000]./1000;
a_vec = ones(1,1000)*a;
tang = w.*VaR+a_vec;
plot(tang,'--');

a_vecj = ones(1,1000)*a_j;
tang_j = w.*VaR_j+a_vecj;
plot(tang_j,'--');

%<---------Breakpoint here

%---------------------------------------------------------
%Apply Lorenz curve on the 2012 DowJones Data
%---------------------------------------------------------
DowJones = readtable('DowJones30.csv');
DJ_data = table2array(DowJones(2:end,2:end));

%Array containig textlabels:
DJ_txt = table2array(readtable('DowJonesList2.csv'));

%Calculate Returns:
R_DJ = ones(250-2,30);
for i=1:30
    r = calcReturn(DJ_data(:,i))-1;
    R_DJ(:,i) = r;
end

DJ_lorenz = ones(249,30);

for i=1:30
    [DJ_lor DJ_rf]=lorenz(R_DJ(:,i));
    DJ_lorenz(:,i) = DJ_lor;
end

for i=1:30
    plot(DJ_lorenz(:,i));
    hold on;
end
axis tight;
xlabel('Cumulative trading days');
ylabel('Lorenz L(p)');
legend(DJ_txt,'Location','NorthWest');
hold off;

%<--------Breakpoint here.

%Calculate the Gini Coefficient for each stock.
%The Gini Coefficient can be computed by 1/2 times the Mean Difference (MD)
for i=1:30
    DJ_gini(i) = MD(R_DJ(:,i))/2;
end

gini_labeled = cell(30,1);
gini_labeled(:,1) = DJ_txt;
gini_dj = num2cell(DJ_gini)';

%Compare to other risk measures:
mean_dj = cell(30,1);
std_dj = cell(30,1);
kurt_dj = cell(30,1);
cvar_dj = cell(30,1);
cvar_dj10 = cell(30,1);
var_dj = cell(30,1);
meanminusgini_dj = cell(30,1);

for i=1:30
    mean_dj(i) = num2cell(mean(R_DJ(:,i)));
    std_dj(i) = num2cell(std(R_DJ(:,i)));
    kurt_dj(i) = num2cell(kurtosis(R_DJ(:,i)));
    cvar_dj(i) = num2cell(CVaR(R_DJ(:,i),0.05));
    cvar_dj10(i) = num2cell(CVaR(R_DJ(:,i),0.1));
    meanminusgini_dj(i) = num2cell(mean(R_DJ(:,i))-DJ_gini(i));
end

%Print a complete table as in Exhibit 3 in the Journal of Portfolio
%Managment.
gini_labeled = horzcat(gini_labeled,mean_dj,gini_dj,meanminusgini_dj,cvar_dj,cvar_dj10);
labels = {'Symbol','Mean','Gini','Mean-Gini','CVaR 5%', 'CVaR 10%'};
gini_labeled2 = vertcat(labels,gini_labeled)

%Sort with respect to Mean-Gini
%In the Journal (Exhibit 4), they order with respect to the mean. For SSD we need both
%rankings to find dominating stocks
%for the presentation I found it more interesting to order w.r.t Mean-Gini
SSD_best = sortrows(horzcat(DJ_txt,mean_dj,meanminusgini_dj),-3);
SSD_labels = {'Symbol','Mean','Mean-Gini'};
SSD_best_labeled = vertcat(SSD_labels,SSD_best)

%Create a "Matrix of Dominance" which shows the stock pairs where one of
%them is SSD.

dominate = zeros(30);
%Dominate contains a zero if there is no stochastic dominance, a 1 if a
%dominates and a -1 if b dominates.
%Stock A --> Row
%Stock B --> Column
%The Matrix is only filled half. The other half would be redundant
%information because it just mirrors the values along the diagonal.
meanminusgini_dj = cell2mat(meanminusgini_dj);
mean_dj = cell2mat(mean_dj);

for i=1:30
    mean_a = mean_dj(i);
    gamma_a = meanminusgini_dj(i);
    for j=1:i
        mean_b = mean_dj(j);
        gamma_b = meanminusgini_dj(j);
        if mean_a>mean_b && gamma_a>gamma_b
            dominate(i,j)=1;
        elseif mean_a<mean_b && gamma_a<gamma_b
            dominate(i,j)=-1;
        end
    end
end

%<--------Breakpoint here

%Plot the Mean and the Mean-Gini
%Red line shows the returns and the green line shows the risk adjusted
%returns.
plot(cell2mat(SSD_best(2:end,2)),'r');
hold on;
plot(cell2mat(SSD_best(2:end,3)),'g');
ylabel('Return');
xlabel('Mean-Gini');
axis tight;
hold off;